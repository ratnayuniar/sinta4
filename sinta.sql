-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 11, 2021 at 12:20 AM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sinta`
--

-- --------------------------------------------------------

--
-- Table structure for table `bks_bahasa`
--

DROP TABLE IF EXISTS `bks_bahasa`;
CREATE TABLE IF NOT EXISTS `bks_bahasa` (
  `id_bks_bhs` int(11) NOT NULL AUTO_INCREMENT,
  `periode` varchar(20) DEFAULT NULL,
  `tahun` varchar(20) DEFAULT NULL,
  `nama_bhs` varchar(20) DEFAULT NULL,
  `skor` varchar(10) DEFAULT NULL,
  `tanggal` varchar(20) DEFAULT NULL,
  `sk_bahasa` varchar(100) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_bhs`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_keterampilan`
--

DROP TABLE IF EXISTS `bks_keterampilan`;
CREATE TABLE IF NOT EXISTS `bks_keterampilan` (
  `id_bks_ket` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ket` varchar(20) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `sk_ket` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_ket`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_organisasi`
--

DROP TABLE IF EXISTS `bks_organisasi`;
CREATE TABLE IF NOT EXISTS `bks_organisasi` (
  `id_bks_org` int(11) NOT NULL AUTO_INCREMENT,
  `nama_org` varchar(50) DEFAULT NULL,
  `tempat` varchar(20) DEFAULT NULL,
  `tahun_masuk` varchar(10) DEFAULT NULL,
  `tahun_keluar` varchar(10) DEFAULT NULL,
  `jabatan` varchar(20) DEFAULT NULL,
  `sk_org` varchar(100) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_org`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_pkl`
--

DROP TABLE IF EXISTS `bks_pkl`;
CREATE TABLE IF NOT EXISTS `bks_pkl` (
  `id_bks_pkl` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) DEFAULT NULL,
  `tempat` varchar(50) DEFAULT NULL,
  `provinsi` varchar(20) DEFAULT NULL,
  `kota` varchar(100) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `ringkasan` varchar(200) DEFAULT NULL,
  `sk_pkl` varchar(50) DEFAULT NULL,
  `laporan` varchar(50) DEFAULT NULL,
  `nim` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  PRIMARY KEY (`id_bks_pkl`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_prestasi`
--

DROP TABLE IF EXISTS `bks_prestasi`;
CREATE TABLE IF NOT EXISTS `bks_prestasi` (
  `id_bks_prestasi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_lomba` varchar(50) DEFAULT NULL,
  `tahun` varchar(20) DEFAULT NULL,
  `juara` varchar(20) DEFAULT NULL,
  `tingkat` varchar(20) DEFAULT NULL,
  `jenis` varchar(20) DEFAULT NULL,
  `piagam` varchar(100) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_prestasi`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_seminar`
--

DROP TABLE IF EXISTS `bks_seminar`;
CREATE TABLE IF NOT EXISTS `bks_seminar` (
  `id_bks_seminar` int(11) NOT NULL AUTO_INCREMENT,
  `nim` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `berita_acara` varchar(225) DEFAULT NULL,
  `persetujuan` varchar(225) DEFAULT NULL,
  `proposal` varchar(225) DEFAULT NULL,
  `presentasi` varchar(225) DEFAULT NULL,
  `monitoring` varchar(225) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_seminar`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_sidang`
--

DROP TABLE IF EXISTS `bks_sidang`;
CREATE TABLE IF NOT EXISTS `bks_sidang` (
  `id_bks_sidang` int(11) NOT NULL AUTO_INCREMENT,
  `nim` int(11) DEFAULT NULL,
  `ukt` int(11) DEFAULT NULL,
  `proposal` varchar(225) DEFAULT NULL,
  `pkkmb` varchar(225) DEFAULT NULL,
  `pengesahan` varchar(225) DEFAULT NULL,
  `monitoring` varchar(225) DEFAULT NULL,
  `persetujuan` varchar(225) DEFAULT NULL,
  `progress` varchar(225) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) NOT NULL,
  `id_prodi` int(11) NOT NULL,
  PRIMARY KEY (`id_bks_sidang`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bks_wisuda`
--

DROP TABLE IF EXISTS `bks_wisuda`;
CREATE TABLE IF NOT EXISTS `bks_wisuda` (
  `id_bks_wisuda` int(11) NOT NULL AUTO_INCREMENT,
  `file_ta` varchar(225) DEFAULT NULL,
  `jurnal` varchar(225) DEFAULT NULL,
  `lap_ta_prodi` varchar(225) DEFAULT NULL,
  `aplikasi` varchar(225) DEFAULT NULL,
  `ppt` varchar(225) DEFAULT NULL,
  `video` varchar(225) DEFAULT NULL,
  `tg_perpus` varchar(225) DEFAULT NULL,
  `ukt` varchar(225) DEFAULT NULL,
  `alat` varchar(225) DEFAULT NULL,
  `foto_ijazah` varchar(225) DEFAULT NULL,
  `foto_wisuda` varchar(225) DEFAULT NULL,
  `kejuaraan` varchar(225) DEFAULT NULL,
  `organisasi` varchar(225) DEFAULT NULL,
  `bahasa` varchar(225) DEFAULT NULL,
  `magang` varchar(225) DEFAULT NULL,
  `ketrampilan` varchar(225) DEFAULT NULL,
  `lap_ta_perpus` varchar(225) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_bks_wisuda`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `detail_topik`
--

DROP TABLE IF EXISTS `detail_topik`;
CREATE TABLE IF NOT EXISTS `detail_topik` (
  `id_detail` int(11) NOT NULL AUTO_INCREMENT,
  `topik_id` int(11) DEFAULT NULL,
  `komentar` text NOT NULL,
  `waktu_komentar` date DEFAULT NULL,
  PRIMARY KEY (`id_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
CREATE TABLE IF NOT EXISTS `dosen` (
  `id_dosen` int(11) NOT NULL AUTO_INCREMENT,
  `nip` bigint(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `no_hp` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id_dosen`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nip`, `nama`, `email`, `no_hp`) VALUES
(16, 100099876545882882, 'Titanisa Aurila', 'titan@gmail.com', '0987654321'),
(17, 98765432123456789, 'Tisya Risma', 'tisya@gmail.com', '08123456789'),
(18, 100099876545882883, 'Anggia Satya', 'anggi@gmail.com', '0088996677');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_seminar`
--

DROP TABLE IF EXISTS `jadwal_seminar`;
CREATE TABLE IF NOT EXISTS `jadwal_seminar` (
  `id_jd_seminar` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mhs` varchar(50) DEFAULT NULL,
  `jadwal` date DEFAULT NULL,
  PRIMARY KEY (`id_jd_seminar`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

DROP TABLE IF EXISTS `jurusan`;
CREATE TABLE IF NOT EXISTS `jurusan` (
  `id_jurusan` int(11) NOT NULL AUTO_INCREMENT,
  `kode_jurusan` varchar(5) DEFAULT NULL,
  `nama_jurusan` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id_jurusan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `kode_jurusan`, `nama_jurusan`) VALUES
(1, '010', 'Komperisasi Akuntansi'),
(2, '020', 'Administrasi Bisnis'),
(4, '030', 'Teknik');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
CREATE TABLE IF NOT EXISTS `mahasiswa` (
  `nama` varchar(50) DEFAULT NULL,
  `nim` int(11) NOT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  PRIMARY KEY (`nim`),
  KEY `id_jurusan` (`id_jurusan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nama`, `nim`, `id_prodi`, `id_jurusan`) VALUES
('Tasya Farasya', 183307018, 4, 2),
('Stefen William', 183307019, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembimbing`
--

DROP TABLE IF EXISTS `pembimbing`;
CREATE TABLE IF NOT EXISTS `pembimbing` (
  `id_pembimbing` int(11) NOT NULL AUTO_INCREMENT,
  `pembimbing1` varchar(100) DEFAULT NULL,
  `pembimbing2` varchar(100) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pembimbing`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembimbing`
--

INSERT INTO `pembimbing` (`id_pembimbing`, `pembimbing1`, `pembimbing2`, `nim`) VALUES
(29, 'Tisya Risma', 'Anggia Satya', 183307018);

-- --------------------------------------------------------

--
-- Table structure for table `penguji`
--

DROP TABLE IF EXISTS `penguji`;
CREATE TABLE IF NOT EXISTS `penguji` (
  `id_penguji` int(11) NOT NULL AUTO_INCREMENT,
  `penguji1` varchar(100) DEFAULT NULL,
  `penguji2` varchar(100) DEFAULT NULL,
  `penguji3` varchar(100) DEFAULT NULL,
  `nim` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_penguji`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penguji`
--

INSERT INTO `penguji` (`id_penguji`, `penguji1`, `penguji2`, `penguji3`, `nim`) VALUES
(7, 'Titanisa Aurila', 'Tisya Risma', 'Anggia Satya', 183307018),
(8, 'Tisya Risma', 'Anggia Satya', 'Titanisa Aurila', 183307019);

-- --------------------------------------------------------

--
-- Table structure for table `perpustakaan`
--

DROP TABLE IF EXISTS `perpustakaan`;
CREATE TABLE IF NOT EXISTS `perpustakaan` (
  `id_perpus` int(11) NOT NULL AUTO_INCREMENT,
  `nim` int(11) DEFAULT NULL,
  `laporan` int(11) DEFAULT NULL,
  `tanggungan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_perpus`),
  KEY `nim` (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prodi`
--

DROP TABLE IF EXISTS `prodi`;
CREATE TABLE IF NOT EXISTS `prodi` (
  `id_prodi` int(11) NOT NULL AUTO_INCREMENT,
  `id_jurusan` int(11) DEFAULT NULL,
  `kode_prodi` varchar(10) DEFAULT NULL,
  `nama_prodi` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_prodi`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prodi`
--

INSERT INTO `prodi` (`id_prodi`, `id_jurusan`, `kode_prodi`, `nama_prodi`) VALUES
(2, 1, '010', 'Komputer Akuntansi'),
(3, 1, '020', 'Akuntansi'),
(4, 4, '0102', 'Bahasa Inggriss'),
(6, 3, '010', 'Teknik Listrik'),
(7, 3, '020', 'Mesin Otomotif'),
(8, 3, '030', 'Teknik Komputer Kontrol'),
(9, 3, '040', 'Teknologi Informasi'),
(10, 3, '050', 'Teknik Perkeretaapian');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Mahasiswa'),
(3, 'Dosen'),
(4, 'Kaprodi'),
(5, 'Keuangan'),
(6, 'Perpustakaan'),
(7, 'BAAK'),
(8, 'LAB'),
(9, 'Bahasa');

-- --------------------------------------------------------

--
-- Table structure for table `topik`
--

DROP TABLE IF EXISTS `topik`;
CREATE TABLE IF NOT EXISTS `topik` (
  `id_topik` int(11) NOT NULL AUTO_INCREMENT,
  `nim` int(11) DEFAULT NULL,
  `bidang` varchar(20) DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `lokasi` varchar(20) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_topik`),
  KEY `nim` (`nim`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topik`
--

INSERT INTO `topik` (`id_topik`, `nim`, `bidang`, `judul`, `lokasi`, `status`, `id_user`, `deskripsi`) VALUES
(5, 183307018, 'Web', 'Aplikasi penjualan online', 'Madiun', 3, 1, 'Untuk melakukan penjualan online');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `image` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `id_role` int(11) DEFAULT NULL,
  `tanggal_buat` int(11) DEFAULT NULL,
  `aktif` int(11) DEFAULT NULL,
  `id_prodi` int(11) DEFAULT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `image`, `password`, `id_role`, `tanggal_buat`, `aktif`, `id_prodi`, `id_jurusan`) VALUES
(1, 'mahasiswa1', 'mahasiswa1@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 2, 1616833531, 1, NULL, 3),
(2, 'Admin 1', 'adminti@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 1, 1616833531, 1, NULL, NULL),
(3, 'Kaprodi1', 'kaprodi1@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 4, 1616833531, 1, NULL, NULL),
(4, 'dosen 1', 'dosen@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 3, 1616833531, 1, NULL, NULL),
(5, 'mahasiswa2', 'mahasiswa2@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 2, 1616833531, 1, NULL, NULL),
(6, 'Perpustakaan1', 'perpus1@gmail.com', 'default.jpg', '$2y$10$pExLI8EkJxnza8jnn8hA5ezzBEXqxSG3/Z.1iUTkesqOKb8ma1v72', 6, 1616833531, 1, NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id_jurusan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perpustakaan`
--
ALTER TABLE `perpustakaan`
  ADD CONSTRAINT `perpustakaan_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `topik`
--
ALTER TABLE `topik`
  ADD CONSTRAINT `topik_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
