<?php
class M_pengguna_mahasiswa extends CI_Model
{

    function tampil_data()
    {

        return $this->db->query("SELECT * FROM user WHERE id_role = '2'");
    }

    function tambah_data()
    {
        $data = array(
            'nama' => $this->input->post('nama'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password'),
            'level' => $this->input->post('level'),
            'status' => $this->input->post('status'),
        );
        $this->db->insert('user', $data);
        redirect('/pengguna_mahasiswa');
    }


    function ubah_data($id_mhs)
    {
        $data = array(
            'nim' => $this->input->post('nim'),
            'nama' => $this->input->post('nama'),
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
        );
        $this->db->where(array('id_mhs' => $id_mhs));
        $this->db->update('mahasiswa', $data);
        redirect('/mahasiswa');
    }


    function hapus_data($id_mhs)
    {
        $this->db->where(array('id_mhs' => $id_mhs));
        $this->db->delete('mahasiswa');
        redirect('/mahasiswa');
    }

    // function tambah_data(){
    // 	$data = array(
    // 		'nim' => $this->input->post('nim'),
    // 		'nama' => $this->input->post('nama'),
    // 		'email' => $this->input->post('email'),
    // 		'password' => $this->input->post('password'),
    // 	);
    // 	$this->db->insert('mahasiswa', $data);
    // 	redirect('../mahasiswa');
    // }

    function input_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function hapus_data2($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // function edit_barang($nim, $nama, $email, $password)
    // {
    // 	$hasil = $this->db->query("UPDATE mahasiswa SET nim='$nim',nama='$nama',email='$email',password='$password' WHERE id='$id'");
    // 	return $hsl;
    // }
}
