<?php
class M_penguji extends CI_Model
{

    function tampil_data()
    {
        return $this->db->get('penguji');
        // return $this->db->query("SELECT * FROM dosen, pembimbing WHERE dosen.id_dosen=pembimbing.pembimbing1");
        // return $this->db->query("SELECT * FROM dosen, pembimbing WHERE dosen.id_dosen=pembimbing.id_pembimbing2");
        // return $this->db->query("SELECT * FROM jurusan, prodi WHERE jurusan.id_jurusan=prodi.id_jurusan");
        // return $this->db->query("SELECT * FROM jurusan, mahasiswa,prodi WHERE jurusan.id_jurusan=mahasiswa.id_jurusan AND prodi.id_prodi=mahasiswa.id_prodi");
        // return $this->db->query("SELECT * FROM dosen, pembimbing,mahasiswa WHERE dosen.id_dosen=pembimbing.id_pembimbing1 AND mahasiswa.nim=pembimbing.id_pembimbing");
    }


    function tambah_data()
    {
        $data = array(
            'nim' => $this->input->post('nim'),
            'penguji1' => $this->input->post('penguji1'),
            'penguji2' => $this->input->post('penguji2'),
            'penguji3' => $this->input->post('penguji3'),
        );
        $this->db->insert('penguji', $data);
        redirect('/penguji');
    }

    function topik_user()
    {
        $this->db->where('penguji.nim', $this->session->userdata('nim'));
        return $this->db->get('penguji');
    }

    function cek_nim($nim)
    {
        $query = array('nim' => $nim);
        return $this->db->get_where('mahasiswa', $query);
    }

    function ubah_data($nim)
    {
        $data = array(
            // 'nim' => $this->input->post('nim'),
            'nama' => $this->input->post('nama'),
            'id_prodi' => $this->input->post('id_prodi'),
            'id_jurusan' => $this->input->post('id_jurusan')
        );
        $this->db->where(array('nim' => $nim));
        $this->db->update('mahasiswa', $data);
        redirect('/mahasiswa');
    }


    function hapus_data($id_penguji)
    {
        $this->db->where(array('id_penguji' => $id_penguji));
        $this->db->delete('penguji');
        redirect('/penguji');
    }




    // function tambah_data(){
    // 	$data = array(
    // 		'nim' => $this->input->post('nim'),
    // 		'nama' => $this->input->post('nama'),
    // 		'prodi' => $this->input->post('prodi'),
    // 		'jurusan' => $this->input->post('jurusan'),
    // 	);
    // 	$this->db->insert('mahasiswa', $data);
    // 	redirect('../mahasiswa');
    // }

    function input_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function hapus_data2($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_id_penguji($id)
    {
        $this->db->where('id_penguji', $id);
        return $this->db->get('penguji');
    }

    function delete($id)
    {
        $this->db->where('id_pembimbing', $id);
        $this->db->delete('pembimbing');
    }
}
