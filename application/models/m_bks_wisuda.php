<?php
class M_bks_wisuda extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_wisuda WHERE mahasiswa.nim=bks_wisuda.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_wisuda', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_wisuda', $id);
        $this->db->update('bks_wisuda', $data);
    }

    function get_nim($id_bks_wisuda)
    {
        $this->db->join('user', 'bks_wisuda.id_user = user.id_user', 'left');
        $this->db->where('id_bks_wisuda', $id_bks_wisuda);

        return $this->db->get('bks_wisuda')->row();
    }

    // function get_nim($nim)
    // {
    // 	$this->db->join('user', 'topik.id_user = user.id_user', 'left');
    // 	$this->db->join('prodi', 'user.id_prodi = prodi.id_prodi', 'left');
    // 	$this->db->join('jurusan', 'user.id_jurusan = jurusan.id_jurusan', 'left');
    // 	$this->db->join('detail_topik', 'topik.id_topik = detail_topik.topik_id', 'left');
    // 	$this->db->join('mahasiswa', 'bks_seminar.id_mhs = mahasiswa.id_mhs', 'left');
    // 	$this->db->where('nim', $nim);

    // 	return $this->db->get('bks_seminar')->row();
    // }



    // function tambah_data(){
    // 	$data = array(
    // 		'nim' => $this->input->post('nim'),
    // 		'nama' => $this->input->post('nama'),
    // 		'prodi' => $this->input->post('prodi'),
    // 		'jurusan' => $this->input->post('jurusan'),
    // 	);
    // 	$this->db->insert('bks_seminar', $data);
    // 	redirect('../bks_seminar');
    // }


    // function ubah_data ($id_mhs){
    // 	$data = array(
    // 		'nim' => $this->input->post('nim'),
    // 		'nama' => $this->input->post('nama'),
    // 		'prodi' => $this->input->post('prodi'),
    // 		'jurusan' => $this->input->post('jurusan')
    // 	   );
    // 		$this->db->where(array('id_mhs'=> $id_mhs));
    // 		$this->db->update('bks_seminar',$data);
    // 		redirect('../bks_seminar');
    // }


    // function hapus_data($id_mhs){
    // 	$this->db->where(array('id_mhs'=> $id_mhs));
    // 	$this->db->delete('bks_seminar');
    // 	redirect('../bks_seminar');
    // }


}
