<?php
class M_dosen extends CI_Model
{

	function tampil_data()
	{
		return $this->db->get('dosen');
	}

	function tambah_data()
	{
		$data = array(
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp'),
		);
		$this->db->insert('dosen', $data);
		redirect('/dosen');
	}


	function ubah_data($id_dosen)
	{
		$data = array(
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp')
		);
		$this->db->where(array('id_dosen' => $id_dosen));
		$this->db->update('dosen', $data);
		redirect('/dosen');
	}


	function hapus_data($id_dosen)
	{
		$this->db->where(array('id_dosen' => $id_dosen));
		$this->db->delete('dosen');
		redirect('/dosen');
	}
}
