<?php
class M_bks_keterampilan extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_keterampilan WHERE mahasiswa.nim=bks_keterampilan.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_keterampilan', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_ket', $id);
        $this->db->update('bks_keterampilan', $data);
    }

    function get_nim($id_bks_ket)
    {
        $this->db->join('user', 'bks_keterampilan.id_user = user.id_user', 'left');
        $this->db->where('id_bks_ket', $id_bks_ket);

        return $this->db->get('bks_keterampilan')->row();
    }
}
