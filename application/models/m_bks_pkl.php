<?php
class M_bks_pkl extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_pkl WHERE mahasiswa.nim=bks_pkl.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_pkl', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_pkl', $id);
        $this->db->update('bks_pkl', $data);
    }

    function get_nim($id_bks_pkl)
    {
        $this->db->join('user', 'bks_pkl.id_user = user.id_user', 'left');
        $this->db->where('id_bks_pkl', $id_bks_pkl);

        return $this->db->get('bks_pkl')->row();
    }
}
