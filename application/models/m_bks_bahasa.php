<?php
class M_bks_bahasa extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_bahasa WHERE mahasiswa.nim=bks_bahasa.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_bahasa', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_bhs', $id);
        $this->db->update('bks_bahasa', $data);
    }

    function get_nim($id_bks_bhs)
    {
        $this->db->join('user', 'bks_bahasa.id_user = user.id_user', 'left');
        $this->db->where('id_bks_bhs', $id_bks_bhs);

        return $this->db->get('bks_bahasa')->row();
    }
}
