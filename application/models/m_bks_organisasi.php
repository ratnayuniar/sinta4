<?php
class M_bks_organisasi extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_organisasi WHERE mahasiswa.nim=bks_organisasi.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_organisasi', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_org', $id);
        $this->db->update('bks_organisasi', $data);
    }

    function get_nim($id_bks_org)
    {
        $this->db->join('user', 'bks_organisasi.id_user = user.id_user', 'left');
        $this->db->where('id_bks_org', $id_bks_org);

        return $this->db->get('bks_organisasi')->row();
    }
}
