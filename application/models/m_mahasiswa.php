<?php
class M_mahasiswa extends CI_Model
{

	function tampil_data()
	{
		// return $this->db->get('mahasiswa');
		// return $this->db->query("SELECT * FROM jurusan, prodi WHERE jurusan.id_jurusan=prodi.id_jurusan");
		return $this->db->query("SELECT * FROM jurusan, mahasiswa,prodi WHERE jurusan.id_jurusan=mahasiswa.id_jurusan AND prodi.id_prodi=mahasiswa.id_prodi");
		// return $this->db->query("SELECT * FROM prodi, mahasiswa WHERE prodi.id_prodi=mahasiswa.id_prodi");
	}


	function tambah_data()
	{
		$data = array(
			'nim' => $this->input->post('nim'),
			'id_jurusan' => $this->input->post('id_jurusan'),
			'id_prodi' => $this->input->post('id_prodi'),
			'nama' => $this->input->post('nama'),
		);
		// print_r($data);
		// exit();
		$this->db->insert('mahasiswa', $data);
		redirect('/mahasiswa');
	}

	function cek_nim($nim)
	{
		$query = array('nim' => $nim);
		return $this->db->get_where('mahasiswa', $query);
	}


	function ubah_data($nim)
	{
		$data = array(
			// 'nim' => $this->input->post('nim'),
			'id_prodi' => $this->input->post('id_prodi'),
			'id_jurusan' => $this->input->post('id_jurusan'),
			'nama' => $this->input->post('nama')
		);
		$this->db->where(array('nim' => $nim));
		$this->db->update('mahasiswa', $data);
		redirect('/mahasiswa');
	}


	function hapus_data($nim)
	{
		$this->db->where(array('nim' => $nim));
		$this->db->delete('mahasiswa');
		redirect('/mahasiswa');
	}

	// function tambah_data(){
	// 	$data = array(
	// 		'nim' => $this->input->post('nim'),
	// 		'nama' => $this->input->post('nama'),
	// 		'prodi' => $this->input->post('prodi'),
	// 		'jurusan' => $this->input->post('jurusan'),
	// 	);
	// 	$this->db->insert('mahasiswa', $data);
	// 	redirect('../mahasiswa');
	// }

	function input_data($data, $table)
	{
		$this->db->insert($table, $data);
	}

	public function hapus_data2($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}

	// function edit_barang($nim, $nama, $prodi, $jurusan)
	// {
	// 	$hasil = $this->db->query("UPDATE mahasiswa SET nim='$nim',nama='$nama',prodi='$prodi',jurusan='$jurusan' WHERE id='$id'");
	// 	return $hsl;
	// }
}
