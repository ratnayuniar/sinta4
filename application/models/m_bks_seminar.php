<?php
class M_bks_seminar extends CI_Model
{

	function tampil_data()
	{
		// return $this->db->get('bks_seminar');
		return $this->db->query("SELECT * FROM mahasiswa, bks_seminar WHERE mahasiswa.nim=bks_seminar.nim");
	}

	function insert($data)
	{
		return $this->db->insert('bks_seminar', $data);
	}

	function update($id, $data)
	{
		$this->db->where('id_bks_seminar', $id);
		$this->db->update('bks_seminar', $data);
	}

	function get_nim($id_bks_seminar)
	{
		$this->db->join('user', 'bks_seminar.id_user = user.id_user', 'left');
		$this->db->where('id_bks_seminar', $id_bks_seminar);

		return $this->db->get('bks_seminar')->row();
	}
}
