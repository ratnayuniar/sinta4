<?php
class M_pembimbing extends CI_Model
{

    function tampil_data()
    {
        return $this->db->get('pembimbing');
        // return $this->db->query("SELECT * FROM dosen, pembimbing WHERE dosen.id_dosen=pembimbing.pembimbing1");
        // return $this->db->query("SELECT * FROM dosen, pembimbing WHERE dosen.id_dosen=pembimbing.id_pembimbing2");
        // return $this->db->query("SELECT * FROM jurusan, prodi WHERE jurusan.id_jurusan=prodi.id_jurusan");
        // return $this->db->query("SELECT * FROM jurusan, mahasiswa,prodi WHERE jurusan.id_jurusan=mahasiswa.id_jurusan AND prodi.id_prodi=mahasiswa.id_prodi");
        // return $this->db->query("SELECT * FROM dosen, pembimbing,mahasiswa WHERE dosen.id_dosen=pembimbing.id_pembimbing1 AND mahasiswa.nim=pembimbing.id_pembimbing");
    }

    // function tampil_data_2()
    // {
    //     return $this->db->query("SELECT * FROM dosen, pembimbing WHERE dosen.id_dosen=pembimbing.pembimbing2");
    //     return $query->result();
    // }

    // function tampil_data_2()
    // {
    //     $this->db->select('dosen, pembimbing');
    //     $this->db->join('dosen', 'pembimbing.pembimbing1 = dosen.id_dosen', 'left');
    //     return $this->db->get('pembimbing')->row();
    // }



    function tambah_data()
    {
        $data = array(
            'nim' => $this->input->post('nim'),
            'pembimbing1' => $this->input->post('pembimbing1'),
            'pembimbing2' => $this->input->post('pembimbing2'),
        );
        $this->db->insert('pembimbing', $data);
        redirect('/pembimbing');
    }

    function cek_nim($nim)
    {
        $query = array('nim' => $nim);
        return $this->db->get_where('mahasiswa', $query);
    }

    function ubah_data($nim)
    {
        $data = array(
            // 'nim' => $this->input->post('nim'),
            'nama' => $this->input->post('nama'),
            'id_prodi' => $this->input->post('id_prodi'),
            'id_jurusan' => $this->input->post('id_jurusan')
        );
        $this->db->where(array('nim' => $nim));
        $this->db->update('mahasiswa', $data);
        redirect('/mahasiswa');
    }


    function hapus_data($id_pembimbing)
    {
        $this->db->where(array('id_pembimbing' => $id_pembimbing));
        $this->db->delete('pembimbing');
        redirect('/pembimbing');
    }

    // function tambah_data(){
    // 	$data = array(
    // 		'nim' => $this->input->post('nim'),
    // 		'nama' => $this->input->post('nama'),
    // 		'prodi' => $this->input->post('prodi'),
    // 		'jurusan' => $this->input->post('jurusan'),
    // 	);
    // 	$this->db->insert('mahasiswa', $data);
    // 	redirect('../mahasiswa');
    // }

    function input_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    public function hapus_data2($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_id_pembimbing($id)
    {
        $this->db->where('id_pembimbing', $id);
        return $this->db->get('pembimbing');
    }

    function delete($id)
    {
        $this->db->where('id_pembimbing', $id);
        $this->db->delete('pembimbing');
    }
}
