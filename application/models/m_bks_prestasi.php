<?php
class M_bks_prestasi extends CI_Model
{

    function tampil_data()
    {
        // return $this->db->get('bks_seminar');
        return $this->db->query("SELECT * FROM mahasiswa, bks_prestasi WHERE mahasiswa.nim=bks_prestasi.nim");
    }

    function insert($data)
    {
        return $this->db->insert('bks_prestasi', $data);
    }

    function update($id, $data)
    {
        $this->db->where('id_bks_prestasi', $id);
        $this->db->update('bks_prestasi', $data);
    }

    function get_nim($id_bks_prestasi)
    {
        $this->db->join('user', 'bks_prestasi.id_user = user.id_user', 'left');
        $this->db->where('id_bks_prestasi', $id_bks_prestasi);

        return $this->db->get('bks_prestasi')->row();
    }
}
