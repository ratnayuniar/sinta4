<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url(); ?>admin/index3.html" class="brand-link">
        <i class="fas fa-graduation-cap mt-2 ml-3 pb-2"></i>
        <span class="brand-text font-weight-light">SINTA</span><B> PNM</B>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?= base_url('admin/dist/img/') . $user['image']; ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="<?php echo base_url(); ?>admin/#" class="d-block"><?php echo $user["nama"]; ?></a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">


            <!--query menu -->



            <a href="" class="brand-link">
                <span class="brand-text font-weight-light">Admin LAB Bahasa</span>
            </a>
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item menu-open">
                    <a href="<?php echo base_url('beranda'); ?>" class="nav-link active">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Beranda
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                        <i class="nav-icon fas fa-database"></i>
                        <p>
                            Data Mahasiswa
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('jadwal_seminar'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jadwal Seminar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('jadwal_sidang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jadwal Sidang</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                        <i class="nav-icon fas fa-clipboard-check"></i>
                        <p>
                            Verifikasi
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('nilai_seminar'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Seminar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('nilai_sidang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Sidang</p>
                            </a>
                        </li>
                    </ul>
                <li class="nav-item">
                    <a href="<?php echo base_url('auth/logout'); ?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>