<?php if ($this->session->userdata('id_role') == 1) { ?>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Berkas Sidang Tugas Akhir</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
              <li class="breadcrumb-item active"> Data Berkas Sidang Tugas Akhir</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div style="text-align:right;margin-bottom: 10px ">
              <!-- <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i> Upload Berkas</a> -->
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Versi Admin Data Berkas Sidang Tugas Akhir</h3>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIM</th>
                      <th>Nama Mahasiswa</th>
                      <th>Status</th>
                      <th>Konfirmasi</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($query->result() as $row) { ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $row->nim ?></td>
                        <td><?= $row->nama ?></td>
                        <td> <?php if ($row->status == '0') {
                                echo '<span class="badge badge-warning">Menunggu</span>';
                              } else if ($row->status == '1') {
                                echo '<span class="badge badge-info">Belum Lengkap</span>';
                              } else if ($row->status == '2') {
                                echo '<span class="badge badge-primary">Kurang Lengkap</span>';
                              } else {
                                echo '<span class="badge badge-danger">Lengkap</span>';
                              }
                              ?>
                        </td>
                        <td>
                          <a href="<?php echo site_url('bks_sidang/save_bks_belum/' . $row->id_bks_sidang); ?>" id="btn-konfirmasi" class="btn btn-xs btn-danger">Belum</a>
                          <a href="<?php echo site_url('bks_sidang/save_bks_kurang/' . $row->id_bks_sidang); ?>" id="btn-konfirmasi" class="btn btn-xs btn-success">Kurang</a>
                          <a href="<?php echo site_url('bks_sidang/save_bks_lengkap/' . $row->id_bks_sidang); ?>" id="btn-konfirmasi" class="btn btn-xs btn-primary">Lengkap</a>
                        </td>
                        <td><a href="<?= base_url('bks_sidang/detail_bks_sidang/' . $row->id_bks_sidang) ?>" class="btn btn-primary btn-sm">
                            <i class="fa fa-search"></i></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Berkas Sidang</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form action="<?= base_url('bks_sidang/create') ?>" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
          <div class="card-body">
            <div class="form-group">
              <input type="hidden" id="id_bks_sidang" name="id_bks_sidang">

              <label for="exampleInputjudul1">NIM</label>
              <input type="text" class="form-control" id="nim" name="nim" placeholder="NIM">
            </div>
            <div class="form-group">
              <label for="berita_acara">Proposal Tugas Akhir</label><br>
              <input type="file" name="proposal">
            </div>
            <div class="form-group">
              <label for="persetujuan">Sertifikat PKKMB</label><br>
              <input type="file" name="pkkmb">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Pengesahan</label><br>
              <input type="file" name="pengesahan">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Monitoring</label><br>
              <input type="file" name="monitoring">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Persetujuan</label><br>
              <input type="file" name="persetujuan">
            </div>
            <!-- <div class="form-group">
              <label for="exampleInputjudul1">Progress</label><br>
              <input type="file" name="monitoring">
            </div> -->
          </div>
          <div class="card-footer">
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div id="delete-modassl" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="custom-width-modalLabel">Konfirmasi Hapus</h4>
        </div>
        <form action="<?php echo base_url() . 'mahasiswa/delete'; ?>" method="post" class="form-horizontal" role="form">
          <div class="modal-body">
            <p>Apakah anda yakin ingin menghapus?</p>
            <div>
              <input type="hidden" id="id_mhs2" name="id_mhs2">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tidak</button>
            <button type="submit" class="btn btn-success waves-effect waves-light">Ya</button>
          </div>
      </div>
    </div>
  </div>

  <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi Hapus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin ingin menghapus?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-outline-light">Ya</button>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).on('click', '#btn-konfirmasi', function(e) {
      e.preventDefault();
      var link = $(this).attr('href');
      Swal.fire({
        title: 'Apakah Anda Yakin ? ',
        text: "Data akan dikonfirmasi ?",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes'
      }).then((result) => {
        if (result.isConfirmed) {
          window.location = link;
          Swal.fire(
            'Sukses',
            'Data berhasil di konfirmasi',
            'success'
          )
        }
      });
    });
  </script>

  <script type="text/javascript">
    function SetInput(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs').value = id_mhs;
      document.getElementById('nim').value = nim;
      document.getElementById('nama').value = nama;
      document.getElementById('prodi').value = prodi;
      document.getElementById('jurusan').value = jurusan;
    }

    function SetInputs(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs2').value = id_mhs;
      document.getElementById('nim2').value = nim;
      document.getElementById('nama2').value = nama;
      document.getElementById('prodi2').value = prodi;
      document.getElementById('jurusan2').value = jurusan;
    }

    function ResetInput(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs').value = "";
      document.getElementById('nim').value = "";
      document.getElementById('nama').value = "";
      document.getElementById('prodi').value = "";
      document.getElementById('jurusan').value = "";
    }
  </script>
<?php } else { ?>

  <!-- PUNYA MAHASISWA -->

  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Berkas Sidang Tugas Akhir</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
              <li class="breadcrumb-item active">Data Berkas Sidang Tugas Akhir</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div style="text-align:right;margin-bottom: 10px ">
              <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i> Upload Berkas</a>
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Data Berkas Sidang Tugas Akhir</h3>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIM</th>
                      <th>Nama Mahasiswa</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($query->result() as $row) { ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $row->nim ?></td>
                        <td><?= $row->nama ?></td>
                        <td> <?php if ($row->status == '0') {
                                echo '<span class="badge badge-warning">Menunggu</span>';
                              } else if ($row->status == '1') {
                                echo '<span class="badge badge-info">Belum Lengkap</span>';
                              } else if ($row->status == '2') {
                                echo '<span class="badge badge-primary">Kurang Lengkap</span>';
                              } else {
                                echo '<span class="badge badge-danger">Lengkap</span>';
                              }
                              ?>
                        </td>
                        <!-- <td><a href="<?php echo site_url('bks_seminar/save_bks_waiting/' . $v->id); ?>" id="btn-konfirmasi" class="btn btn-xs btn-danger">Konfirmasi</a></td> -->
                        <td><a href="<?= base_url('bks_sidang/detail_bks_sidang/' . $row->id_bks_sidang) ?>" class="btn btn-primary btn-sm">
                            <i class="fa fa-search"></i></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Berkas Sidang Tugas Akhir MHS</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form action="<?= base_url('bks_sidang/create') ?>" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
          <div class="card-body">
            <div class="form-group">
              <input type="hidden" id="id_bks_sidang" name="id_bks_sidang">

              <label for="exampleInputjudul1">NIM</label>
              <input type="text" class="form-control" id="nim" name="nim" placeholder="NIM">
            </div>
            <div class="form-group">
              <label for="berita_acara">Proposal Tugas Akhir</label><br>
              <input type="file" name="proposal">
            </div>
            <div class="form-group">
              <label for="persetujuan">Sertifikat PKKMB</label><br>
              <input type="file" name="pkkmb">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Pengesahan</label><br>
              <input type="file" name="pengesahan">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Monitoring</label><br>
              <input type="file" name="monitoring">
            </div>
            <div class="form-group">
              <label for="exampleInputjudul1">Persetujuan</label><br>
              <input type="file" name="persetujuan">
            </div>
            <!-- <div class="form-group">
              <label for="exampleInputjudul1">Progress</label><br>
              <input type="file" name="monitoring">
            </div> -->
          </div>
          <div class="card-footer">
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>


  <div id="delete-modassl" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title" id="custom-width-modalLabel">Konfirmasi Hapus</h4>
        </div>
        <form action="<?php echo base_url() . 'mahasiswa/delete'; ?>" method="post" class="form-horizontal" role="form">
          <div class="modal-body">
            <p>Apakah anda yakin ingin menghapus?</p>
            <div>
              <input type="hidden" id="id_mhs2" name="id_mhs2">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tidak</button>
            <button type="submit" class="btn btn-success waves-effect waves-light">Ya</button>
          </div>
      </div>
    </div>
  </div>

  <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
      <div class="modal-content bg-danger">
        <div class="modal-header">
          <h4 class="modal-title">Konfirmasi Hapus</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Apakah anda yakin ingin menghapus?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-outline-light">Ya</button>
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript">
    function SetInput(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs').value = id_mhs;
      document.getElementById('nim').value = nim;
      document.getElementById('nama').value = nama;
      document.getElementById('prodi').value = prodi;
      document.getElementById('jurusan').value = jurusan;
    }

    function SetInputs(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs2').value = id_mhs;
      document.getElementById('nim2').value = nim;
      document.getElementById('nama2').value = nama;
      document.getElementById('prodi2').value = prodi;
      document.getElementById('jurusan2').value = jurusan;
    }

    function ResetInput(id_mhs, nim, nama, prodi, jurusan) {
      document.getElementById('id_mhs').value = "";
      document.getElementById('nim').value = "";
      document.getElementById('nama').value = "";
      document.getElementById('prodi').value = "";
      document.getElementById('jurusan').value = "";
    }
  </script>
<?php } ?>