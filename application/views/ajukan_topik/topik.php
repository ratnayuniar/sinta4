<!-- USER KAPRODI -->
<?php if ($this->session->userdata('id_role') == 4) { ?>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar Topik Tugas Akhir</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar Topik Tugas Akhir</h3>
            </div>
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Bidang</th>
                    <th>Judul</th>
                    <th>Lokasi</th>
                    <th>Status</th>
                    <th>Konfirmasi</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($query as $row) { ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $row->nim ?></td>
                      <td><?= $row->bidang ?></td>
                      <td><?= $row->judul ?></td>
                      <td><?= $row->lokasi ?></td>
                      <td>
                        <?php if ($row->status == '0') {
                          echo '<span class="badge badge-warning">Menunggu</span>';
                        } else if ($row->status == '1') {
                          echo '<span class="badge badge-info">Telah Dikonfirmasi</span>';
                        } else if ($row->status == '2') {
                          echo '<span class="badge badge-primary">Proses</span>';
                        } else {
                          echo '<span class="badge badge-danger">Disetujui</span>';
                        }
                        ?>
                      </td>
                      <td>
                        <?php
                        if ($row->status == '0') {
                          echo '<a href="javascript:void(0);" data-toggle="modal" data-target="#modal-topik" id="select_topik"
                          data-id_topik="' . $row->id_topik . '"
                          data-status="' . $row->status . '"
                          class="btn btn-warning btn-sm">
                          Konfirmasi
                          </a>';
                        } else if ($row->status == '1') {
                          echo '<a href="javascript:void(0);" data-toggle="modal" data-target="#modal-reply" id="reply-message"
                          data-topik_id="' . $row->id_topik . '"
                          data-id_topik_id="' . $row->id_topik . '"
                          data-judul="' . $row->judul . '"
                          data-deskripsi="' . $row->deskripsi . '"
                          class="btn btn-info btn-sm">
                          Komentari
                          </a>';
                        } else if ($row->status == '2') {
                          echo '<a href="javascript:void(0);" data-toggle="modal" data-target="#modalclosetopik" id="ctopik"
                          data-closetopik="' . $row->id_topik . '"
                          data-closestatus="' . $row->status . '"                          
                          class="btn btn-primary btn-sm">
                          Setujui
                          </a>';
                        } else {
                          echo '<a href="javascript:void(0);" class="btn btn-danger btn-sm">
                          Disetujui
                          </a>';
                        }
                        ?>
                      </td>
                      <td>
                        <a href="<?= base_url('topik/detail_topik/' . $row->id_topik) ?>" class="btn btn-primary btn-sm">
                          <i class="fa fa-search"></i>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="modal fade" id="modal-topik">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Yakin konfirmasi topik?</h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('topik/save_topik_waiting') ?>" method="POST" enctype="multipart/form-data">

            <div class="modal-body">
              <input type="hidden" name="id_topik" id="id_topik" class="form-control">
              <input type="hidden" name="status" value="1" class="form-control">
            </div>

            <button type="submit" id="tombol" class="btn btn-primary btn-sm">Ya</button>
            <button type="reset" class="btn btn-danger btn-sm">Tidak</button>

          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-reply">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Komentar Topik</h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('topik/save_komentar') ?>" method="POST" enctype="multipart/form-data">
            <div class="modal-body">
              <input type="hidden" name="id_topik" id="id_topik_id" class="form-control">
              <input type="hidden" name="topik_id" id="topik_id" class="form-control">
              <div class="form-group">
                <label for="judul">Judul Topik</label>
                <input type="text" id="judul" class="form-control" readonly>
              </div>
              <div class="form-group">
                <label for="deskripsi">Deskripsi</label>
                <textarea id="deskripsi" class="form-control" readonly></textarea>
              </div>
              <div class="form-group">
                <label for="deskripsi">Komentar</label>
                <textarea name="komentar" class="form-control"></textarea>
              </div>
            </div>

            <button type="submit" id="tombol" class="btn btn-primary btn-sm">Kirim</button>
            <button type="reset" class="btn btn-danger btn-sm">Batal</button>

          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalclosetopik">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Yakin Menyetujui topik?</h5>
          <button class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('topik/save_close_topik') ?>" method="POST" enctype="multipart/form-data">

            <div class="modal-body">
              <input type="hidden" name="id_topik" id="closetopik" class="form-control">
              <input type="hidden" name="status" value="3" class="form-control">
            </div>

            <button type="submit" id="tombol" class="btn btn-primary btn-sm">Ya</button>
            <button type="reset" class="btn btn-danger btn-sm">Tidak</button>

          </form>
        </div>
      </div>
    </div>
  </div>

  <script>
    $(document).ready(function() {
      $(document).on('click', '#select_topik', function() {
        var id_topik = $(this).data('id_topik');
        var status = $(this).data('status');

        $('#id_topik').val(id_topik);
        $('#status').val(status);
      })

      $(document).on('click', '#reply-message', function() {
        var id_topik = $(this).data('id_topik');
        var id_topik_id = $(this).data('id_topik_id');
        var judul = $(this).data('judul');
        var deskripsi = $(this).data('deskripsi');

        $('#id_topik').val(id_topik);
        $('#id_topik_id').val(id_topik_id);
        $('#judul').val(judul);
        $('#deskripsi').val(deskripsi);
      })

      $(document).on('click', '#ctopik', function() {
        var closetopik = $(this).data('closetopik');
        var closestatus = $(this).data('closestatus');

        $('#closetopik').val(closetopik);
        $('#closestatus').val(closestatus);

      })
    })
  </script>

  <script type="text/javascript">
    function SetInput(id_topik, nim, bidang, judul, lokasi) {
      document.getElementById('id_topik').value = id_topik;
      document.getElementById('nim').value = nim;
      document.getElementById('bidang').value = bidang;
      document.getElementById('judul').value = judul;
      document.getElementById('lokasi').value = lokasi;
    }

    function SetInputs(id_topik, nim, bidang, judul, lokasi) {
      document.getElementById('id_topik2').value = id_topik;
      document.getElementById('nim2').value = nim;
      document.getElementById('bidang2').value = bidang;
      document.getElementById('judul2').value = judul;
      document.getElementById('lokasi2').value = lokasi;
    }

    function ResetInput(id_topik, nim, bidang, judul, lokasi) {
      document.getElementById('id_topik').value = "";
      document.getElementById('nim').value = "";
      document.getElementById('bidang').value = "";
      document.getElementById('judul').value = "";
      document.getElementById('lokasi').value = "";
    }
  </script>

  <!-- USER MAHASISWA -->

<?php } else { ?>
  <div class="content-wrapper">
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ajukan Judul</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog" style="width:55%;">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title">Ajukan Topik Mahasiswa</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <form action="<?php echo base_url() . 'topik/add'; ?>" method="post" class="form-horizontal" role="form">
                  <div class="card-body">
                    <div class="form-group">
                      <input type="hidden" id="id_topik" name="id_topik">

                      <label for="exampleInputjudul1">NIM</label>
                      <input type="text" class="form-control" id="nim" name="nim" placeholder="NIM">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputjudul1">Bidang</label>
                      <input type="text" class="form-control" id="bidang" name="bidang" placeholder="Bidang">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputjudul1">Topik Tugas Akhir</label>
                      <textarea class="form-control" rows="3" id="judul" name="judul" placeholder="Masukkan Topik Tugas Akhir"></textarea>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputjudul1">Lokasi</label>
                      <input type="text" class="form-control" id="lokasi" name="lokasi" placeholder="Lokasi ">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputjudul1">Deskripsi</label>
                      <textarea class="form-control" rows="3" id="deskripsi" name="deskripsi" placeholder="Masukkan Deskripsi Tugas Akhir"></textarea>
                    </div>
                  </div>

                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div style="text-align:right;margin-bottom: 10px ">
              <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i> Ajukan Judul</a>
            </div>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Topik Tugas Akhir</h3>
              </div>
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>NIM</th>
                      <th>Bidang</th>
                      <th>Judul</th>
                      <th>Lokasi</th>
                      <th>Status</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                    foreach ($topik_user->result() as $row) { ?>
                      <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $row->nim ?></td>
                        <td><?= $row->bidang ?></td>
                        <td><?= $row->judul ?></td>
                        <td><?= $row->lokasi ?></td>
                        <td>
                          <?php if ($row->status == '0') {
                            echo '<span class="badge badge-warning">Menunggu</span>';
                          } else if ($row->status == '1') {
                            echo '<span class="badge badge-info">Telah Dikonfirmasi</span>';
                          } else if ($row->status == '2') {
                            echo '<span class="badge badge-primary">Proses</span>';
                          } else {
                            echo '<span class="badge badge-danger">Disetujui</span>';
                          }
                          ?>
                        </td>
                        <td>
                          <a href="<?= base_url('topik/detail_topik/' . $row->id_topik) ?>" class="btn btn-primary btn-sm">
                            <i class="fa fa-search"></i>
                            <a onclick="return confirm('Yakin akan hapus?');" href="<?= base_url('topik/delete_topik/' . $row->id_topik) ?>" class="btn btn-danger btn-sm">
                              <i class="fa fa-trash"></i>
                            </a>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
          <div class="modal-dialog">
            <div class="modal-content bg-danger">
              <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <form action="<?php echo base_url() . 'topik/delete'; ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                  <p>Apakah anda yakin ingin menghapus?</p>
                  <div>
                    <input type="hidden" id="id_topik2" name="id_topik2">
                  </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                  <button type="submit" class="btn btn-outline-light">Ya</button>
                </div>
            </div>
          </div>
        </div>
    </section>
  </div>
  <script type="text/javascript">
    function SetInput(id_topik, nim, bidang, judul, lokasi, deskripsi) {
      document.getElementById('id_topik').value = id_topik;
      document.getElementById('nim').value = nim;
      document.getElementById('bidang').value = bidang;
      document.getElementById('judul').value = judul;
      document.getElementById('lokasi').value = lokasi;
      document.getElementById('deskripsi').value = deskripsi;
    }

    function SetInputs(id_topik, nim, bidang, judul, lokasi, deskripsi) {
      document.getElementById('id_topik2').value = id_topik;
      document.getElementById('nim2').value = nim;
      document.getElementById('bidang2').value = bidang;
      document.getElementById('judul2').value = judul;
      document.getElementById('lokasi2').value = lokasi;
      document.getElementById('deskripsi2').value = deskripsi;
    }

    function ResetInput(id_topik, nim, bidang, judul, lokasi, deskripsi) {
      document.getElementById('id_topik').value = "";
      document.getElementById('nim').value = "";
      document.getElementById('bidang').value = "";
      document.getElementById('judul').value = "";
      document.getElementById('lokasi').value = "";
      document.getElementById('deskripsi').value = "";
    }
  </script>
<?php } ?>