<!-- Content Wrapper. Contains page content -->
<?php if ($this->session->userdata('id_role') == 4) { ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Daftar Topik Tugas Akhir</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">General Form</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="callout callout-info">
                            <h5>NIM <?= $topik->nim ?></h5>
                        </div>


                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-book"></i> Pengajuan Topik
                                        <!-- <small class="float-right">Date: <?= $topik->tanggal_ajukan; ?></small> -->
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    From
                                    <address>
                                        <strong><?= $topik->nama; ?></strong><br>
                                        Program Studi : <?= $topik->nama_prodi; ?><br>
                                        Jurusan : <?= $topik->nama_jurusan; ?><br>
                                        Email: <?= $topik->email; ?>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>Status Topik</b> : <?php if ($topik->status == '0') {
                                                                echo '<span class="badge badge-warning">Menunggu</span>';
                                                            } else if ($topik->status == '1') {
                                                                echo '<span class="badge badge-info">Komentari</span>';
                                                            } else if ($topik->status == '2') {
                                                                echo '<span class="badge badge-primary">Setujui</span>';
                                                            } else {
                                                                echo '<span class="badge badge-danger">Disetujui</span>';
                                                            }
                                                            ?>

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- Table row -->
                            <div class="row">
                                <div class="col-6">
                                    <label for="">Topik</label>
                                    <input type="text" value="<?= $topik->judul ?>" readonly class="form-control">
                                    <label for="">Bidang</label>
                                    <input type="text" value="<?= $topik->bidang ?>" readonly class="form-control">
                                    <label for="">Lokasi</label>
                                    <input type="text" value="<?= $topik->lokasi ?>" readonly class="form-control">
                                    <label for="">Deskripsi</label>
                                    <textarea class="form-control" readonly rows="6"><?= $topik->deskripsi ?></textarea>
                                </div>
                                <div class="col-6">
                                    <label for="">Komentar</label>
                                    <textarea class="form-control" readonly rows="9"><?= $topik->komentar ?></textarea>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'topik/delete'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus?</p>
                        <div>
                            <input type="hidden" id="id_topik2" name="id_topik2">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-outline-light">Ya</button>
                    </div>
            </div>

            <!-- /.modal-content -->
        </div>
    </div>
    <script type="text/javascript">
        function SetInput(id_topik, nim, bidang, judul, lokasi) {
            document.getElementById('id_topik').value = id_topik;
            document.getElementById('nim').value = nim;
            document.getElementById('bidang').value = bidang;
            document.getElementById('judul').value = judul;
            document.getElementById('lokasi').value = lokasi;
        }

        function SetInputs(id_topik, nim, bidang, judul, lokasi) {
            document.getElementById('id_topik2').value = id_topik;
            document.getElementById('nim2').value = nim;
            document.getElementById('bidang2').value = bidang;
            document.getElementById('judul2').value = judul;
            document.getElementById('lokasi2').value = lokasi;
        }

        function ResetInput(id_topik, nim, bidang, judul, lokasi) {
            document.getElementById('id_topik').value = "";
            document.getElementById('nim').value = "";
            document.getElementById('bidang').value = "";
            document.getElementById('judul').value = "";
            document.getElementById('lokasi').value = "";
        }
    </script>
<?php } else { ?>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Daftar Topik Tugas Akhir</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">General Form</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="callout callout-info">
                            <h5>NIM <?= $topik->nim ?></h5>
                        </div>


                        <!-- Main content -->
                        <div class="invoice p-3 mb-3">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-12">
                                    <h4>
                                        <i class="fas fa-book"></i> Pengajuan Topik
                                        <!-- <small class="float-right">Date: <?= $topik->tanggal_ajukan; ?></small> -->
                                    </h4>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <div class="row invoice-info">
                                <div class="col-sm-4 invoice-col">
                                    From
                                    <address>
                                        <strong><?= $topik->nama; ?></strong><br>
                                        Program Studi : <?= $topik->nama_prodi; ?><br>
                                        Jurusan : <?= $topik->nama_jurusan; ?><br>
                                        Email: <?= $topik->email; ?>
                                    </address>
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 invoice-col">
                                    <b>Status Topik</b> : <?php if ($topik->status == '0') {
                                                                echo '<span class="badge badge-warning">Menunggu</span>';
                                                            } else if ($topik->status == '1') {
                                                                echo '<span class="badge badge-info">Komentari</span>';
                                                            } else if ($topik->status == '2') {
                                                                echo '<span class="badge badge-primary">Setujui</span>';
                                                            } else {
                                                                echo '<span class="badge badge-danger">Disetujui</span>';
                                                            }
                                                            ?>

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->

                            <!-- Table row -->
                            <div class="row">
                                <div class="col-6">
                                    <label for="">Topik</label>
                                    <input type="text" value="<?= $topik->judul ?>" readonly class="form-control">
                                    <label for="">Bidang</label>
                                    <input type="text" value="<?= $topik->bidang ?>" readonly class="form-control">
                                    <label for="">Lokasi</label>
                                    <input type="text" value="<?= $topik->lokasi ?>" readonly class="form-control">
                                    <label for="">Deskripsi</label>
                                    <textarea class="form-control" readonly rows="6"><?= $topik->deskripsi ?></textarea>
                                </div>
                                <div class="col-6">
                                    <label for="">Komentar</label>
                                    <textarea class="form-control" readonly rows="9"><?= $topik->komentar ?></textarea>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <!-- /.invoice -->
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>

        <!-- /.content -->
    </div>
    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'topik/delete'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus?</p>
                        <div>
                            <input type="hidden" id="id_topik2" name="id_topik2">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-outline-light">Ya</button>
                    </div>
            </div>

            <!-- /.modal-content -->
        </div>
    </div>
    <script type="text/javascript">
        function SetInput(id_topik, nim, bidang, judul, lokasi, deskripsi) {
            document.getElementById('id_topik').value = id_topik;
            document.getElementById('nim').value = nim;
            document.getElementById('bidang').value = bidang;
            document.getElementById('judul').value = judul;
            document.getElementById('lokasi').value = lokasi;
            document.getElementById('deskripsi').value = deskripsi;
        }

        function SetInputs(id_topik, nim, bidang, judul, lokasi, deskripsi) {
            document.getElementById('id_topik2').value = id_topik;
            document.getElementById('nim2').value = nim;
            document.getElementById('bidang2').value = bidang;
            document.getElementById('judul2').value = judul;
            document.getElementById('lokasi2').value = lokasi;
            document.getElementById('deskripsi2').value = deskripsi;
        }

        function ResetInput(id_topik, nim, bidang, judul, lokasi, deskripsi) {
            document.getElementById('id_topik').value = "";
            document.getElementById('nim').value = "";
            document.getElementById('bidang').value = "";
            document.getElementById('judul').value = "";
            document.getElementById('lokasi').value = "";
            document.getElementById('deskripsi').value = "";
        }
    </script>
<?php } ?>