<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Data Pembimbing</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
                        <li class="breadcrumb-item active">Data Pembimbing</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Data Pembimbing</h3>
                        </div>
                        <div class="card-body">
                            <div style="text-align:right;margin-bottom: 10px ">
                                <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i></a>
                            </div>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIM</th>
                                        <th>Dosen Pembimbing 1</th>
                                        <th>Dosen Pembimbing 2</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($query->result() as $row) {
                                        echo
                                        "<tr>
											<td>" . $no . "</td>
											<td>" . $row->nim . "</td>
											<td>" . $row->pembimbing1 . "</td>
											<td>" . $row->pembimbing2 . "</td>
											<td><a href ='#' class ='on-default edit-row btn btn-primary' data-toggle='modal' data-target='#custom-width-modal' onClick=\"SetInput('" . $row->id_pembimbing . "','" . $row->nim . "','" . $row->pembimbing1 . "','" . $row->pembimbing2 . "')\"><i class ='fa fa-edit'></i></a>
												<a href ='#' class ='on-default remove-row btn btn-danger' data-toggle='modal' data-target='#delete-modal'onClick=\"SetInputs('" . $row->id_pembimbing . "','" . $row->nim . "','" . $row->pembimbing1 . "','" . $row->pembimbing2 . "')\"><i class ='fa fa-trash'></i></a>
											</td>
									    </tr>";
                                        $no++;
                                    }
                                    ?>
                                    <!-- <?php $no = 1;
                                            foreach ($query->result() as $key => $value) { ?>
                                        <tr>
                                            <td><?= $no++; ?></td>
                                            <td><?= $value->nim ?></td>
                                            <td><?= $value->pembimbing1 ?></td>
                                            <td><?= $value->pembimbing2 ?></td>
                                            <td>
                                                <a onclick="return confirm('Yakin akan hapus?');" href="<?= base_url('pembimbing/delete_pembimbing/' . $row->id_pembimbing) ?>" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php } ?> -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah Data Dosen Pembimbing</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?php echo base_url() . 'pembimbing/add'; ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id_pembimbing" name="id_pembimbing">
                        <label class="col-md-3 control-label">NIM</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="nim" name="nim">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">Dosen Pembimbing 1</label>
                        <div class="col-md-9">
                            <select class="form-control" data-live-search="true" data-style="btn-white" onclick="choose()" id="nama" name="" required>
                                <option>-- Pilih Dosen Pembimbing 1 --</option>
                                <?php
                                $query = $this->m_dosen->tampil_data();
                                foreach ($query->result() as $row) {
                                    echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="pembimbing1" id="output" class="form-control" value="" readonly />
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">Dosen Pembimbing 2</label>
                        <div class="col-md-9">
                            <select class="form-control" data-live-search="true" data-style="btn-white" onclick="pilih()" id="nama2" name="" required>
                                <option>-- Pilih Dosen Pembimbing 2 --</option>
                                <?php
                                $query = $this->m_dosen->tampil_data();
                                foreach ($query->result() as $row) {
                                    echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="hidden" name="pembimbing2" id="hasil" class="form-control" value="" readonly />
                    </div>

                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>



<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?php echo base_url() . 'pembimbing/delete'; ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <p>Apakah anda yakin ingin menghapus?</p>
                    <div>
                        <input type="hidden" id="id_pembimbing2" name="id_pembimbing2">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-outline-light">Ya</button>
                </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function choose() {
        var zoo = document.getElementById('nama').value;
        document.getElementById('output').value = zoo;
    }
</script>

<script type="text/javascript">
    function pilih() {
        var zoo = document.getElementById('nama2').value;
        document.getElementById('hasil').value = zoo;
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#pembimbing1').on('change', function() {
            var id_dosen = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('pembimbing/get_id_dosen') ?>",
                dataType: "JSON",
                data: {
                    id_dosen: id_dosen
                },
                cache: false,
                success: function(data) {
                    $.each(data, function($id_dosen, nama) {
                        $('[name="id_dosen"]').val(data.id_dosen);
                        $('[name="nama_pembimbing1"]').val(data.nama);


                    });
                }
            });
            return false;
        });
    });
</script>

<script type="text/javascript">
    function SetInput(id_pembimbing, nim, pembimbing1, pembimbing2) {
        document.getElementById('id_pembimbing').value = id_pembimbing;
        document.getElementById('nim').value = nim;
        document.getElementById('pembimbing1').value = pembimbing1;
        document.getElementById('pembimbing2').value = pembimbing2;
    }

    function SetInputs(id_pembimbing, nim, pembimbing1, pembimbing2) {

        document.getElementById('id_pembimbing2').value = id_pembimbing;
        document.getElementById('nim2').value = nim;
        document.getElementById('pembimbing12').value = pembimbing1;
        document.getElementById('pembimbing22').value = pembimbing2;

    }

    function ResetInput() {
        document.getElementById('id_jurusan').value = "";
        document.getElementById('id_prodi').value = "";
        document.getElementById('nama_prodi').value = "";
        document.getElementById('kode_prodi').value = "";
    }
</script>