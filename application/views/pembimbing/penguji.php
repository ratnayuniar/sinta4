<?php if ($this->session->userdata('id_role') == 1) { ?>
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Penguji</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
                            <li class="breadcrumb-item active">Data Penguji</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Penguji</h3>
                            </div>
                            <div class="card-body">
                                <div style="text-align:right;margin-bottom: 10px ">
                                    <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i></a>
                                </div>
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIM</th>
                                            <th>Dosen Penguji 1</th>
                                            <th>Dosen Penguji 2</th>
                                            <th>Dosen Penguji 3</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($query->result() as $row) {
                                            echo
                                            "<tr>
											<td>" . $no . "</td>
											<td>" . $row->nim . "</td>
											<td>" . $row->penguji1 . "</td>
											<td>" . $row->penguji2 . "</td>
											<td>" . $row->penguji3 . "</td>
                                            <td><a href ='#' class ='on-default edit-row btn btn-primary' data-toggle='modal' data-target='#custom-width-modal' onClick=\"SetInput('" . $row->id_penguji . "','" . $row->nim . "','" . $row->penguji1 . "','" . $row->penguji2 . "','" . $row->penguji3 . "')\"><i class ='fa fa-edit'></i></a>
													<a href ='#' class ='on-default remove-row btn btn-danger' data-toggle='modal' data-target='#delete-modal'onClick=\"SetInputs('" . $row->id_penguji . "','" . $row->nim . "','" . $row->penguji1 . "','" . $row->penguji2 . "','" . $row->penguji3 . "')\"><i class ='fa fa-trash'></i></a>
											</td>
											
									    </tr>";
                                            $no++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Dosen Penguji</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'penguji/add'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" id="id_penguji" name="id_penguji">
                            <label class="col-md-3 control-label">NIM</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nim" name="nim">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 1</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="choose()" id="nama" name="" required>
                                    <option>-- Pilih Dosen Penguji 1 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji1" id="output" class="form-control" value="" readonly />
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 2</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="pilih()" id="nama2" name="" required>
                                    <option>-- Pilih Dosen Penguji 2 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji2" id="hasil" class="form-control" value="" readonly />
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 3</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="pilih2()" id="nama3" name="" required>
                                    <option>-- Pilih Dosen Penguji 3 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji3" id="hasil3" class="form-control" value="" readonly />
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'penguji/delete'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus?</p>
                        <div>
                            <input type="hidden" id="id_penguji2" name="id_penguji2">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-outline-light">Ya</button>
                    </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function choose() {
            var zoo = document.getElementById('nama').value;
            document.getElementById('output').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function pilih() {
            var zoo = document.getElementById('nama2').value;
            document.getElementById('hasil').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function pilih2() {
            var zoo = document.getElementById('nama3').value;
            document.getElementById('hasil3').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function SetInput(id_jurusan, id_prodi, nama_prodi, kode_prodi) {
            document.getElementById('id_jurusan').value = id_jurusan;
            document.getElementById('id_prodi').value = id_prodi;
            document.getElementById('nama_prodi').value = nama_prodi;
            document.getElementById('kode_prodi').value = kode_prodi;
        }

        function SetInputs(id_penguji, nim, penguji1, penguji2, penguji3) {

            document.getElementById('id_penguji2').value = id_penguji;
            document.getElementById('nim2').value = nim;
            document.getElementById('penguji12').value = penguji1;
            document.getElementById('penguji22').value = penguji2;
            document.getElementById('penguji32').value = penguji3;

        }

        function ResetInput() {
            document.getElementById('id_jurusan').value = "";
            document.getElementById('id_prodi').value = "";
            document.getElementById('nama_prodi').value = "";
            document.getElementById('kode_prodi').value = "";
        }
    </script>

<?php } else { ?>

    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Data Penguji</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
                            <li class="breadcrumb-item active">Data Penguji</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Penguji</h3>
                            </div>
                            <div class="card-body">
                                <!-- <div style="text-align:right;margin-bottom: 10px ">
                                    <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i></a>
                                </div> -->
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIM</th>
                                            <th>Dosen Penguji 1</th>
                                            <th>Dosen Penguji 2</th>
                                            <th>Dosen Penguji 3</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        foreach ($topik_user->result() as $row) {
                                            echo
                                            "<tr>
											<td>" . $no . "</td>
											<td>" . $row->nim . "</td>
											<td>" . $row->penguji1 . "</td>
											<td>" . $row->penguji2 . "</td>
											<td>" . $row->penguji3 . "</td>
                                            <td><a href ='#' class ='on-default edit-row btn btn-primary' data-toggle='modal' data-target='#custom-width-modal' onClick=\"SetInput('" . $row->id_penguji . "','" . $row->nim . "','" . $row->penguji1 . "','" . $row->penguji2 . "','" . $row->penguji3 . "')\"><i class ='fa fa-edit'></i></a>
													<a href ='#' class ='on-default remove-row btn btn-danger' data-toggle='modal' data-target='#delete-modal'onClick=\"SetInputs('" . $row->id_penguji . "','" . $row->nim . "','" . $row->penguji1 . "','" . $row->penguji2 . "','" . $row->penguji3 . "')\"><i class ='fa fa-trash'></i></a>
											</td>
											
									    </tr>";
                                            $no++;
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" style="width:55%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data Dosen Penguji</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'penguji/add'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="hidden" id="id_penguji" name="id_penguji">
                            <label class="col-md-3 control-label">NIM</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" id="nim" name="nim">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 1</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="choose()" id="nama" name="" required>
                                    <option>-- Pilih Dosen Penguji 1 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji1" id="output" class="form-control" value="" readonly />
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 2</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="pilih()" id="nama2" name="" required>
                                    <option>-- Pilih Dosen Penguji 2 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji2" id="hasil" class="form-control" value="" readonly />
                        </div>

                        <div class="form-group">
                            <label class="col-md-5 control-label">Dosen Penguji 3</label>
                            <div class="col-md-9">
                                <select class="form-control" data-live-search="true" data-style="btn-white" onclick="pilih2()" id="nama3" name="" required>
                                    <option>-- Pilih Dosen Penguji 3 --</option>
                                    <?php
                                    $query = $this->m_dosen->tampil_data();
                                    foreach ($query->result() as $row) {
                                        echo "<option value='" . $row->nama . "'>" . $row->nama . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="penguji3" id="hasil3" class="form-control" value="" readonly />
                        </div>

                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content bg-danger">
                <div class="modal-header">
                    <h4 class="modal-title">Konfirmasi Hapus</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="<?php echo base_url() . 'penguji/delete'; ?>" method="post" class="form-horizontal" role="form">
                    <div class="modal-body">
                        <p>Apakah anda yakin ingin menghapus?</p>
                        <div>
                            <input type="hidden" id="id_penguji2" name="id_penguji2">
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                        <button type="submit" class="btn btn-outline-light">Ya</button>
                    </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function choose() {
            var zoo = document.getElementById('nama').value;
            document.getElementById('output').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function pilih() {
            var zoo = document.getElementById('nama2').value;
            document.getElementById('hasil').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function pilih2() {
            var zoo = document.getElementById('nama3').value;
            document.getElementById('hasil3').value = zoo;
        }
    </script>

    <script type="text/javascript">
        function SetInput(id_jurusan, id_prodi, nama_prodi, kode_prodi) {
            document.getElementById('id_jurusan').value = id_jurusan;
            document.getElementById('id_prodi').value = id_prodi;
            document.getElementById('nama_prodi').value = nama_prodi;
            document.getElementById('kode_prodi').value = kode_prodi;
        }

        function SetInputs(id_penguji, nim, penguji1, penguji2, penguji3) {

            document.getElementById('id_penguji2').value = id_penguji;
            document.getElementById('nim2').value = nim;
            document.getElementById('penguji12').value = penguji1;
            document.getElementById('penguji22').value = penguji2;
            document.getElementById('penguji32').value = penguji3;

        }

        function ResetInput() {
            document.getElementById('id_jurusan').value = "";
            document.getElementById('id_prodi').value = "";
            document.getElementById('nama_prodi').value = "";
            document.getElementById('kode_prodi').value = "";
        }
    </script>
<?php } ?>