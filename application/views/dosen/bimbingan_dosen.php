  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <!-- /.content-header -->
      <!-- Main content -->
      <section class="content">
          <div class="container-fluid">
              <!-- Small boxes (Stat box) -->

              <div class="row">
                  <div class="col-md-6">
                      <div class="card card-primary">
                          <div class="card-header">
                              <h3 class="card-title">Bimbingan Tugas Akhir</h3>
                          </div>
                          <!-- /.card-header -->
                          <!-- form start -->
                          <form>
                              <div class="card-body">
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Masalah Yang Dikonsultasikan</label>
                                      <textarea class="form-control" rows="3" placeholder=""></textarea>
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputEmail1">Solusi Yang Diberikan</label>
                                      <textarea class="form-control" rows="3" placeholder=""></textarea>
                                  </div>
                              </div>
                              <!-- /.card-body -->

                              <div class="card-footer">
                                  <button type="submit" class="btn btn-primary">Kirim</button>
                              </div>
                          </form>
                      </div>


                      <!-- /.row -->
                      <!-- Main row -->

                      <!-- /.row (main row) -->
                  </div>
              </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->