<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url(); ?>admin/index3.html" class="brand-link">
        <i class="fas fa-graduation-cap mt-2 ml-3 pb-2"></i>
        <span class="brand-text font-weight-light">SINTA</span><B> PNM</B>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <!-- <img src="<?= base_url('admin/dist/img/') . $session['image']; ?>" class="img-circle elevation-2" alt="User Image"> -->

            </div>
            <div class="info">
                <a href="<?php echo base_url('profile'); ?>" class="d-block"><?= $this->session->nama; ?></a>
            </div>
        </div>

        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                        <i class="fas fa-search fa-fw"></i>
                    </button>
                </div>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">

            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

                <li class="nav-item">
                    <a href="<?php echo base_url('beranda'); ?>" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Beranda
                        </p>
                    </a>
                </li>
                <?php if ($this->session->userdata('id_role') == 4) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url('topik'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                Topik Tugas Akhir
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('beranda'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Bimbingan
                            </p>
                        </a>
                    </li>
                <?php } ?>
                <?php if ($this->session->userdata('id_role') == 2) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url('topik'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-book-open"></i>
                            <p>
                                Topik Tugas Akhir
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="<?php echo base_url('beranda'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Bimbingan
                            </p>
                        </a>
                    </li>
                <?php } ?>
                <?php if ($this->session->userdata('id_role') == 3) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url('beranda'); ?>" class="nav-link">
                            <i class="nav-icon fas fa-chalkboard-teacher"></i>
                            <p>
                                Bimbingan
                            </p>
                        </a>
                    </li>
                <?php } ?>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                        <i class="nav-icon fas fa-users-cog"></i>
                        <p>
                            Penguji & Pembimbing
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('penguji'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dosen Penguji</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('pembimbing'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Dosen Pembimbing</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <?php if ($this->session->userdata('id_role') == 1) { ?>
                    <li class="nav-item">
                        <a href="#" class="nav-link ">
                            <i class="nav-icon fas fa-users"></i>
                            <p>
                                Pengguna
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url('pengguna_dosen'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Dosen</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('pengguna_mahasiswa'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Mahasiswa</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->session->userdata('id_role') == 1) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                            <i class="nav-icon fas fa-database"></i>
                            <p>
                                Master Data
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url('mahasiswa'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Mahasiswa</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('dosen'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Dosen</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('jurusan'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Jurusan</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('prodi2'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Data Prodi</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php } ?>
                <?php if ($this->session->userdata('id_role') == 1) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                            <i class="nav-icon fas fa-file-archive"></i>
                            <p>
                                Berkas
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_seminar'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Sempro</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_sidang'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Sidang</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_wisuda'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Wisuda</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php }  ?>

                <?php if ($this->session->userdata('id_role') == 2) { ?>
                    <li class="nav-item">
                        <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                            <i class="nav-icon fas fa-file-archive"></i>
                            <p>
                                Berkas
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_seminar'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Sempro</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_sidang'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Sidang</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?php echo base_url('bks_wisuda'); ?>" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Berkas Wisuda</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php }  ?>

                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                        <i class="nav-icon fas fa-calendar-alt"></i>
                        <p>
                            Jadwal
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('jadwal_seminar'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jadwal Seminar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('jadwal_sidang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Jadwal Sidang</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="<?php echo base_url(); ?>admin/#" class="nav-link ">
                        <i class="nav-icon fas fa-clipboard"></i>
                        <p>
                            Nilai
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo base_url('nilai_seminar'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Seminar</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo base_url('nilai_sidang'); ?>" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Nilai Sidang</p>
                            </a>
                        </li>
                    </ul>
                <li class="nav-item">
                    <a href="<?php echo base_url('auth/logout'); ?>" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            Logout
                        </p>
                    </a>
                </li>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>