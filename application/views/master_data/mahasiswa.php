<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Data Mahasiswa</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
            <li class="breadcrumb-item active">Data Mahasiswa</li>
          </ol>
        </div>
      </div>
    </div>
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Mahasiswa</h3>
            </div>
            <div class="card-body">
              <div style="text-align:right;margin-bottom: 10px ">
                <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i></a>
              </div>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NIM</th>
                    <th>Nama Mahasiswa</th>
                    <th>Program Studi</th>
                    <th>Jurusan</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach ($query->result() as $row) {
                    echo
                    "<tr>
											<td>" . $no . "</td>
											<td>" . $row->nim . "</td>
											<td>" . $row->nama . "</td>
											<td>" . $row->nama_prodi . "</td>
											<td>" . $row->nama_jurusan . "</td>
											<td><a href ='#' class ='on-default edit-row btn btn-primary' data-toggle='modal' data-target='#custom-width-modal' onClick=\"SetInput('" . $row->nim . "','" . $row->nama . "','" . $row->id_prodi . "','" . $row->id_jurusan . "')\"><i class ='fa fa-edit'></i></a>
													<a href ='#' class ='on-default remove-row btn btn-danger' data-toggle='modal' data-target='#delete-modal'onClick=\"SetInputs('" . $row->nim . "','" . $row->nama . "','" . $row->id_prodi . "','" . $row->id_jurusan . "')\"><i class ='fa fa-trash'></i></a>
											</td>
									  </tr>";
                    $no++;
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:55%;">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Mahasiswa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="<?php echo base_url() . 'mahasiswa/add'; ?>" method="post" class="form-horizontal" role="form">
        <div class="modal-body">

          <div class="form-group">
            <label class="col-md-3 control-label">NIM</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nim" name="nim" required>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-5 control-label">Nama Mahasiswa</label>
            <div class="col-md-9">
              <input type="text" class="form-control" id="nama" name="nama" required>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Prodi</label>
            <div class="col-md-9">
              <select class="form-control" data-live-search="true" data-style="btn-white" id="id_prodi" name="id_prodi" required>
                <option>-- Pilih Program Studi --</option>
                <?php
                $query = $this->m_prodi2->tampil_data();
                foreach ($query->result() as $row) {
                  echo "<option value='" . $row->id_prodi . "'>" . $row->nama_prodi . "</option>";
                }
                ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label">Jurusan</label>
            <div class="col-md-9">
              <select class="form-control" data-live-search="true" data-style="btn-white" id="id_jurusan" name="id_jurusan" required>
                <option>-- Pilih Jurusan --</option>
                <?php
                $query = $this->m_jurusan->tampil_data();
                foreach ($query->result() as $row) {
                  echo "<option value='" . $row->id_jurusan . "'>" . $row->nama_jurusan . "</option>";
                }
                ?>
              </select>
            </div>
          </div>

          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </div>

        </div>
      </form>
    </div>
  </div>
</div>

<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content bg-danger">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi Hapus</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <form action="<?php echo base_url() . 'mahasiswa/delete'; ?>" method="post" class="form-horizontal" role="form">
        <div class="modal-body">
          <p>Apakah anda yakin ingin menghapus?</p>
          <div>
            <input type="hidden" id="nim2" name="nim2">
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
          <button type="submit" class="btn btn-outline-light">Ya</button>
        </div>
    </div>
  </div>
</div>


<script type="text/javascript">
  function SetInput(nim, nama, id_jurusan, id_prodi) {
    document.getElementById('nim').value = nim;
    document.getElementById('nama').value = nama;
    document.getElementById('id_prodi').value = id_prodi;
    document.getElementById('id_jurusan').value = id_jurusan;
  }

  function SetInputs(id_jurusan, id_prodi, nim, nama) {

    document.getElementById('nim2').value = nim;
    document.getElementById('nama').value = nama;
  }

  function ResetInput() {
    document.getElementById('nim').value = "";
    document.getElementById('nama').value = "";
    document.getElementById('id_prodi').value = "";
    document.getElementById('id_jurusan').value = "";
  }
</script>