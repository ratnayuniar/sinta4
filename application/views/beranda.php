  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <h3 class="m-2">Selamat Datang, <?php echo $user["nama"]; ?></h3>
        <h5 class="m-2">Terdaftar <?= date('d F Y', $user['tanggal_buat']); ?></h5>
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card mb-2 bg-gradient-dark">
            <img class="card-img-top" src="<?= base_url('admin/dist/img/pnm.jpg'); ?>" alt="Dist Photo 1">
            <div class="card-img-overlay d-flex flex-column justify-content-end">
              <!-- <h2 class="card-title text-primary text-black">POLITEKNIK NEGERI MADIUN</h2>
              <!-- <p class="card-text text-black pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p> -->
              <!-- <a href="#" class="text-white">Last update 2 mins ago</a> -->
            </div>
          </div>
        </div>
        <div class="row">

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>1500</h3>

                <p>Data Mahasiswa</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>30</h3>

                <p>Data Dosen</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>3</h3>

                <p>Data Jurusan</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->

          <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->

        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->