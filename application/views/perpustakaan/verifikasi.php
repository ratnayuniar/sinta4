<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Verifikasi</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
                        <li class="breadcrumb-item active">Verifikasi</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- /.card -->
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Verifikasi Data</h3>
                        </div>
                        <div style="text-align:right; margin-right:15px">

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div style="text-align:right;margin-bottom: 10px ">
                                <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right" data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus"></i></a>
                            </div>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>NIM</th>
                                        <th>Nama</th>
                                        <th>Program Studi</th>
                                        <th>Laporan TA</th>
                                        <th>Tanggungan Perpustakaan</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 1;
                                    foreach ($query->result() as $row) { ?>
                                        <tr>
                                            <td><?= $no++ ?></td>
                                            <td><?= $row->nim ?></td>
                                            <td><?= $row->nama ?></td>
                                            <td><?= $row->nama_prodi ?></td>
                                            <td>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_belum/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-danger">Belum</a>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_kurang/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-success">Kurang</a>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_lengkap/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-primary">Lengkap</a>
                                            </td>
                                            <td>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_belum/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-danger">Belum</a>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_kurang/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-success">Kurang</a>
                                                <a href="<?php echo site_url('bks_wisuda/save_bks_lengkap/' . $row->nim); ?>" id="btn-konfirmasi" class="btn btn-xs btn-primary">Lengkap</a>
                                            </td>
                                            <td><a href="<?= base_url('bks_wisuda/detail_bks_wisuda/' . $row->nim) ?>" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-search"></i>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>


                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </section>
    <!-- /.content -->
</div>


<div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Data Jurusan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="<?php echo base_url() . 'jurusan/add'; ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" id="id_jurusan" name="id_jurusan">
                        <label class="col-md-3 control-label">Kode Jurusan</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="kode_jurusan" name="kode_jurusan" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Nama Jurusan</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="nama_jurusan" name="nama_jurusan" required>
                        </div>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>

                    </div>

                </div>
            </form>
        </div>

    </div>
</div>


<div id="delete-modassl" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:55%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="custom-width-modalLabel">Konfirmasi Hapus</h4>
            </div>
            <form action="<?php echo base_url() . 'jurusan/delete'; ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <p>Apakah anda yakin ingin menghapus?</p>
                    <div>
                        <input type="hidden" id="id_jurusan2" name="id_jurusan2">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tidak</button>
                    <button type="submit" class="btn btn-success waves-effect waves-light">Ya</button>
                </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title">Konfirmasi Hapus</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-outline-light">Ya</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
</div>


<script type="text/javascript">
    function SetInput(id_jurusan, kode_jurusan, nama_jurusan, email, no_hp) {
        document.getElementById('id_jurusan').value = id_jurusan;
        document.getElementById('kode_jurusan').value = kode_jurusan;
        document.getElementById('nama_jurusan').value = nama_jurusan;
    }

    function SetInputs(id_jurusan, kode_jurusan, nama_jurusan, email, no_hp) {
        document.getElementById('id_jurusan2').value = id_jurusan;
        document.getElementById('kode_jurusan2').value = kode_jurusan;
        document.getElementById('nama_jurusan2').value = nama_jurusan;
    }

    function ResetInput(id_jurusan, kode_jurusan, nama_jurusan, email, no_hp) {
        document.getElementById('id_jurusan').value = "";
        document.getElementById('kode_jurusan').value = "";
        document.getElementById('nama_jurusan').value = "";
    }
</script>