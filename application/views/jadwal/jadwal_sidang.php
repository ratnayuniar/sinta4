<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jadwal Ujian Sidang Tugas Akhir</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>admin/#">Home</a></li>
              <li class="breadcrumb-item active">Jadwal Ujian Sidang Tugas Akhir</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jadwal Ujian Sidang Tugas Akhir</h3>
              </div>
							<div style="text-align:right; margin-right:15px">
                
							</div>
              <!-- /.card-header -->
              <div class="card-body">
                <div style="text-align:right;margin-bottom: 10px ">
                  <a href="#" class="on-default edit-row btn btn-success pull-right" data-toggle="modal" pull="right"data-target="#custom-width-modal" onclick="ResetInput()"><i class="fa fa-plus" ></i></a>
                </div>
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Mahasiswa</th>
                    <th>Jadwal</th>
										<th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php 
										$no = 1;
										foreach($query->result() as $row){
											echo "<tr>
											<td>".$no."</td>
                      <td>".$row->id_jurusan."</td>
											<td>".$row->kode_prodi."</td>
											<td>".$row->nama_prodi."</td>
											<td>
                          <a href ='#' class ='on-default edit-row btn btn-primary' data-toggle='modal' data-target='#custom-width-modal' onClick=\"SetInput('".$row->id_prodi."','".$row->id_jurusan."','".$row->kode_prodi."','".$row->nama_prodi."')\"><i class ='fa fa-edit'></i></a>
													<a href ='#' class ='on-default remove-row btn btn-danger' data-toggle='modal' data-target='#delete-modal'onClick=\"SetInputs('".$row->id_prodi."','".$row->id_jurusan."','".$row->kode_prodi."','".$row->nama_prodi."')\"><i class ='fa fa-trash'></i></a>
											</td>

											
									</tr>";
									$no++;
													
									} 
									?>
                  </tbody>

                 
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->

    </section>
    <!-- /.content -->
  </div>


 <div id="custom-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:55%;">
<div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Tambah Jadwal Ujian Sidang</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
           
            <div class="card-body">
                <!-- Date -->
                <div class="form-group">
                  <label>Nama Mahasiswa</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                        
                    </div>
                </div>

                <div class="form-group">
                  <label>Jadwal</label>
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-target="#reservationdate">
                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
                <!-- /.form group -->
                <!-- Date range -->

                <!-- /.form group -->

                <!-- Date and time range -->

                <!-- /.form group -->

                <!-- Date and time range -->

                <!-- /.form group -->
              </div>

               <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
              <button type="submit" class="btn btn-primary">Simpan</button>
              
            </div>

            </div>
           
          </div>
        </div>


        <div id="delete-modassl" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog" style="width:55%;">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                        <h4 class="modal-title" id="custom-width-modalLabel">Konfirmasi Hapus</h4>
                                                    </div>
                                                   <form action="<?php echo base_url(). 'jurusan/delete'; ?>" method="post" class="form-horizontal" role="form">
                                                    <div class="modal-body">
                                                        <p>Apakah anda yakin ingin menghapus?</p>
                                                            <div>
                                                 <input type="hidden" id="id_prodi2" name="id_prodi2">
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tidak</button>
                                                        <button type="submit" class="btn btn-success waves-effect waves-light">Ya</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->     


<div id="delete-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="custom-width-modalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
          <div class="modal-content bg-danger">
            <div class="modal-header">
              <h4 class="modal-title">Konfirmasi Hapus</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Apakah anda yakin ingin menghapus?</p>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-outline-light" data-dismiss="modal">Tidak</button>
              <button type="submit" class="btn btn-outline-light">Ya</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
      </div>


<script type="text/javascript">
    function SetInput(id_prodi, kode_prodi, nama_prodi, email,no_hp){
        document.getElementById('id_prodi').value = id_prodi;
        document.getElementById('kode_prodi').value = kode_prodi;
        document.getElementById('nama_prodi').value = nama_prodi;
    }
    function SetInputs(id_prodi, kode_prodi, nama_prodi, email,no_hp){
        document.getElementById('id_prodi2').value = id_prodi;
        document.getElementById('kode_prodi2').value = kode_prodi;
        document.getElementById('nama_prodi2').value = nama_prodi;
    }

    function ResetInput(id_prodi, kode_prodi, nama_prodi, email,no_hp){
        document.getElementById('id_prodi').value = "";
        document.getElementById('kode_prodi').value = "";
        document.getElementById('nama_prodi').value = "";
    }
   
</script>