<div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
        <div class="card-header text-center">
            <a href="../../index2.html" class="h1"><b>SINTA</b></a>
        </div>
        <div class="card-body">
            <p class="login-box-msg">Sign In</p>

            <?= $this->session->flashdata('message'); ?>

            <form action="<?= base_url('auth/proses_login'); ?>" method="post">
                <div class="input-group mb-3">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?= set_value('email'); ?>">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>
                <?= form_error('email', '<small class="text-danger">', '</small>'); ?>
                <div class="input-group mb-3">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>
                <?= form_error('password', '<small class="text-danger">', '</small>'); ?>
                <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
            </form>


            <p class="mb-1">
                <a href="forgot-password.html">Lupa Kata Sandi?</a>
            </p>
            <p class="mb-0">
                <a href="<?= base_url('auth/registrasi'); ?>" class="text-center">Aktivasi Akun</a>
            </p>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
</div>
<!-- /.login-box -->