<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nilai_seminar extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jadwal_seminar');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['query'] = $this->m_jadwal_seminar->tampil_data();
		$data['title'] = 'SINTA PNM';
		$data['user'] = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('nilai/nilai_seminar', $data);
		$this->load->view('templates/footer', $data);
	}
}
