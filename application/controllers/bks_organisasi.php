<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_organisasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_organisasi');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_organisasi->tampil_data();

        // $data['bks_keterampilan_user'] = $this->m_bks_keterampilan->bks_keterampilan_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_organisasi')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_organisasi', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_organisasi->update($id, ['status' => 1]);
        redirect('bks_organisasi', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_organisasi->update($id, ['status' => 2]);
        redirect('bks_organisasi', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_organisasi->update($id, ['status' => 3]);
        redirect('bks_organisasi', 'refresh');
    }

    function detail_bks_organisasi($nim)
    {
        $data['bks_organisasi'] = $this->m_bks_organisasi->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_organisasi']) {
            $data['title'] = 'Detail Berkas' . $data['bks_organisasi']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_organisasi', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $this->form_validation->set_rules('nama_org', 'Nama Organisasi', 'required');
            $this->form_validation->set_rules('tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('tahun_masuk', 'Tahun Masuk', 'required');
            $this->form_validation->set_rules('tahun_keluar', 'Tahun Keluar', 'required');
            $this->form_validation->set_rules('jabatan', 'Jabatan', 'required');
            $config['upload_path'] = './assets/berkas/organisasi/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['sk_org'])) {
                $this->upload->do_upload('sk_org');
                $data1 = $this->upload->data();
                $sk_org = $data1['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $nama_org = $this->input->post('nama_org', TRUE);
                $tempat = $this->input->post('tempat', TRUE);
                $tahun_masuk = $this->input->post('tahun_masuk', TRUE);
                $tahun_keluar = $this->input->post('tahun_keluar', TRUE);
                $jabatan = $this->input->post('jabatan', TRUE);
                $data = [
                    'nim' => $nim,
                    'nama_org' => $nama_org,
                    'tempat' => $tempat,
                    'tahun_masuk' => $tahun_masuk,
                    'tahun_keluar' => $tahun_keluar,
                    'jabatan' => $jabatan,
                    'sk_org' => $sk_org,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();
                // var_dump($data);
                $insert = $this->db->insert('bks_organisasi', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_organisasi');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
