<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_wisuda extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_wisuda');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_wisuda->tampil_data();

        // $data['bks_seminar_user'] = $this->m_bks_wisuda->bks_seminar_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_wisuda')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_wisuda', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_wisuda->update($id, ['status' => 1]);
        redirect('bks_wisuda', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_wisuda->update($id, ['status' => 2]);
        redirect('bks_wisuda', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_wisuda->update($id, ['status' => 3]);
        redirect('bks_wisuda', 'refresh');
    }

    function detail_bks_wisuda($nim)
    {
        $data['bks_wisuda'] = $this->m_bks_wisuda->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_wisuda']) {
            $data['title'] = 'Detail Berkas' . $data['bks_wisuda']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_wisuda', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $config['upload_path'] = './assets/berkas/wisuda/';
            $config['allowed_types'] = 'pdf|jpg|png|jpeg|mp4';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['file_ta'])) {
                $this->upload->do_upload('file_ta');
                $data1 = $this->upload->data();
                $file_ta = $data1['file_name'];
            }

            if (!empty($_FILES['jurnal'])) {
                $this->upload->do_upload('jurnal');
                $data2 = $this->upload->data();
                $jurnal = $data2['file_name'];
            }

            if (!empty($_FILES['lap_ta_prodi'])) {
                $this->upload->do_upload('lap_ta_prodi');
                $data3 = $this->upload->data();
                $lap_ta_prodi = $data3['file_name'];
            }

            if (!empty($_FILES['aplikasi'])) {
                $this->upload->do_upload('aplikasi');
                $data4 = $this->upload->data();
                $aplikasi = $data4['file_name'];
            }

            if (!empty($_FILES['ppt'])) {
                $this->upload->do_upload('ppt');
                $data5 = $this->upload->data();
                $ppt = $data5['file_name'];
            }

            if (!empty($_FILES['video'])) {
                $this->upload->do_upload('video');
                $data6 = $this->upload->data();
                $video = $data6['file_name'];
            }

            if (!empty($_FILES['foto_ijazah'])) {
                $this->upload->do_upload('foto_ijazah');
                $data7 = $this->upload->data();
                $foto_ijazah = $data7['file_name'];
            }

            if (!empty($_FILES['foto_wisuda'])) {
                $this->upload->do_upload('foto_wisuda');
                $data8 = $this->upload->data();
                $foto_wisuda = $data8['file_name'];
            }

            if (!empty($_FILES['kejuaraan'])) {
                $this->upload->do_upload('kejuaraan');
                $data9 = $this->upload->data();
                $kejuaraan = $data9['file_name'];
            }

            if (!empty($_FILES['organisasi'])) {
                $this->upload->do_upload('organisasi');
                $data10 = $this->upload->data();
                $organisasi = $data10['file_name'];
            }

            if (!empty($_FILES['magang'])) {
                $this->upload->do_upload('magang');
                $data11 = $this->upload->data();
                $magang = $data11['file_name'];
            }

            if (!empty($_FILES['ketrampilan'])) {
                $this->upload->do_upload('ketrampilan');
                $data12 = $this->upload->data();
                $ketrampilan = $data12['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $data = [
                    'nim' => $nim,
                    'file_ta' => $file_ta,
                    'jurnal' => $jurnal,
                    'lap_ta_prodi' => $lap_ta_prodi,
                    'aplikasi' => $aplikasi,
                    'ppt' => $ppt,
                    'video' => $video,
                    'foto_ijazah' => $foto_ijazah,
                    'foto_wisuda' => $foto_wisuda,
                    'kejuaraan' => $kejuaraan,
                    'organisasi' => $organisasi,
                    'magang' => $magang,
                    'ketrampilan' => $ketrampilan,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();

                $insert = $this->db->insert('bks_wisuda', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_wisuda');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
