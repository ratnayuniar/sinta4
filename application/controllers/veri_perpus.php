<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Veri_perpus extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_veri_perpus');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_veri_perpus->tampil_data();

        // $data['bks_seminar_user'] = $this->m_veri_perpus->bks_seminar_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_wisuda')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('perpustakaan/verifikasi', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_veri_perpus->update($id, ['status' => 1]);
        redirect('bks_wisuda', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_veri_perpus->update($id, ['status' => 2]);
        redirect('bks_wisuda', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_veri_perpus->update($id, ['status' => 3]);
        redirect('bks_wisuda', 'refresh');
    }

    function detail_bks_wisuda($nim)
    {
        $data['bks_wisuda'] = $this->m_veri_perpus->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_wisuda']) {
            $data['title'] = 'Detail Berkas' . $data['bks_wisuda']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_wisuda', $data);
            $this->load->view('templates/footer', $data);
        }
    }
}
