<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_keterampilan extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_keterampilan');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_keterampilan->tampil_data();

        // $data['bks_keterampilan_user'] = $this->m_bks_keterampilan->bks_keterampilan_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_keterampilan')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_keterampilan', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_keterampilan->update($id, ['status' => 1]);
        redirect('bks_keterampilan', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_keterampilan->update($id, ['status' => 2]);
        redirect('bks_keterampilan', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_keterampilan->update($id, ['status' => 3]);
        redirect('bks_keterampilan', 'refresh');
    }

    function detail_bks_ket($nim)
    {
        $data['bks_keterampilan'] = $this->m_bks_keterampilan->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_keterampilan']) {
            $data['title'] = 'Detail Berkas' . $data['bks_keterampilan']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_ket', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $this->form_validation->set_rules('nama_ket', 'Nama', 'required');
            $this->form_validation->set_rules('jenis', 'Jenis', 'required');
            $this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
            $config['upload_path'] = './assets/berkas/keterampilan/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['sk_ket'])) {
                $this->upload->do_upload('sk_ket');
                $data1 = $this->upload->data();
                $sk_ket = $data1['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $nama_ket = $this->input->post('nama_ket', TRUE);
                $jenis = $this->input->post('jenis', TRUE);
                $tingkat = $this->input->post('tingkat', TRUE);
                $data = [
                    'nim' => $nim,
                    'nama_ket' => $nama_ket,
                    'jenis' => $jenis,
                    'tingkat' => $tingkat,
                    'sk_ket' => $sk_ket,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();
                // var_dump($data);
                $insert = $this->db->insert('bks_keterampilan', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_keterampilan');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
