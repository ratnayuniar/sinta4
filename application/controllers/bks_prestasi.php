<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_prestasi extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_prestasi');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_prestasi->tampil_data();

        // $data['bks_keterampilan_user'] = $this->m_bks_keterampilan->bks_keterampilan_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_prestasi')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_prestasi', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_prestasi->update($id, ['status' => 1]);
        redirect('bks_prestasi', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_prestasi->update($id, ['status' => 2]);
        redirect('bks_prestasi', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_prestasi->update($id, ['status' => 3]);
        redirect('bks_prestasi', 'refresh');
    }

    function detail_bks_prestasi($nim)
    {
        $data['bks_prestasi'] = $this->m_bks_prestasi->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_prestasi']) {
            $data['title'] = 'Detail Berkas' . $data['bks_prestasi']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_prestasi', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $this->form_validation->set_rules('nama_lomba', 'Nama Lomba', 'required');
            $this->form_validation->set_rules('tahun', 'Tahun', 'required');
            $this->form_validation->set_rules('juara', 'Juara', 'required');
            $this->form_validation->set_rules('tingkat', 'Tingkat', 'required');
            $this->form_validation->set_rules('jenis', 'Jenis', 'required');
            $config['upload_path'] = './assets/berkas/prestasi/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['piagam'])) {
                $this->upload->do_upload('piagam');
                $data1 = $this->upload->data();
                $piagam = $data1['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $nama_lomba = $this->input->post('nama_lomba', TRUE);
                $tahun = $this->input->post('tahun', TRUE);
                $juara = $this->input->post('juara', TRUE);
                $tingkat = $this->input->post('tingkat', TRUE);
                $jenis = $this->input->post('jenis', TRUE);
                $data = [
                    'nim' => $nim,
                    'nama_lomba' => $nama_lomba,
                    'tahun' => $tahun,
                    'juara' => $juara,
                    'tingkat' => $tingkat,
                    'jenis' => $jenis,
                    'piagam' => $piagam,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();
                // var_dump($data);
                $insert = $this->db->insert('bks_prestasi', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_keterampilan');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
