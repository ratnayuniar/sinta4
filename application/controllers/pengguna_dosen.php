<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengguna_dosen extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_pengguna_dosen');
        $this->load->helper('url');
    }

    public function index()
    {
        $data['query'] = $this->m_pengguna_dosen->tampil_data();
        $data['title'] = 'SINTA PNM';
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pengguna/dosen', $data);
        $this->load->view('templates/footer', $data);
    }

    public function add()
    {
        $id_user = $this->input->post('id_user');

        if (empty($id_user)) $this->m_pengguna_dosen->tambah_data();
        else $this->m_pengguna_dosen->ubah_data($id_user);
    }

    public function delete()
    {
        $id_user = $this->input->post('id_user2');
        $this->m_pengguna_dosen->hapus_data($id_user);
    }
}
