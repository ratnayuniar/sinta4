<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dosen extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_dosen');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['query'] = $this->m_dosen->tampil_data();
		$data['title'] = 'SINTA PNM';
		$data['user'] = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('master_data/dosen', $data);
		$this->load->view('templates/footer', $data);
	}

	public function add()
	{
		$id_dosen = $this->input->post('id_dosen');

		if (empty($id_dosen)) $this->m_dosen->tambah_data();
		else $this->m_dosen->ubah_data($id_dosen);
	}

	public function delete()
	{
		$id_dosen = $this->input->post('id_dosen2');
		$this->m_dosen->hapus_data($id_dosen);
	}
}
