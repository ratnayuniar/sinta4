<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mahasiswa extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jurusan');
		$this->load->model('m_prodi2');
		$this->load->model('m_mahasiswa');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['query'] = $this->m_mahasiswa->tampil_data();

		$data['title'] = 'SINTA PNM';
		$data['user'] = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('master_data/mahasiswa', $data);
		$this->load->view('templates/footer', $data);
	}

	public function add()
	{
		$id_nim = $this->input->post('nim');

		$query = $this->m_mahasiswa->cek_nim($id_nim)->num_rows();
		if (empty($query))
			$this->m_mahasiswa->tambah_data();
		else
			$this->m_mahasiswa->ubah_data($id_nim);
	}

	public function delete()
	{
		$nim = $this->input->post('nim2');
		$this->m_mahasiswa->hapus_data($nim);
	}
}
