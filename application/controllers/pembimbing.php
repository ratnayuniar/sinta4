<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembimbing extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_dosen');
        $this->load->model('m_pembimbing');
        $this->load->helper('url');
    }

    public function index()
    {
        $data['query'] = $this->m_pembimbing->tampil_data();
        $data['title'] = 'SINTA PNM';
        // $data['query2'] = $this->m_pembimbing->tampil_data_2();
        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pembimbing/pembimbing', $data);
        $this->load->view('templates/footer', $data);
    }

    public function add()
    {
        $id_pembimbing = $this->input->post('id_pembimbing');

        if (empty($id_pembimbing)) $this->m_pembimbing->tambah_data();
        else $this->m_pembimbing->ubah_data($id_pembimbing);
    }

    public function delete()
    {
        $id_pembimbing = $this->input->post('id_pembimbing2');
        $this->m_pembimbing->hapus_data($id_pembimbing);
    }

    function delete_pembimbing($id)
    {
        $delete = $this->m_pembimbing->get_id_pembimbing($id);
        if ($delete) {
            $this->m_pembimbing->delete($id);
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Data Berhasil di Hapus</div>');
            redirect('pembimbing', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Data Tidak ada</div>');
            redirect('pembimbing', 'refresh');
        }
    }

    function get_id_dosen()
    {
        $id_dosen = $this->input->post('id_dosen');
        $data = $this->m_pembimbing->get_id_pembimbing($id_dosen);
        echo json_encode($data);
    }
}
