<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_bahasa extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_bahasa');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_bahasa->tampil_data();

        // $data['bks_keterampilan_user'] = $this->m_bks_keterampilan->bks_keterampilan_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_bahasa')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_bahasa', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_bahasa->update($id, ['status' => 1]);
        redirect('bks_bahasa', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_bahasa->update($id, ['status' => 2]);
        redirect('bks_bahasa', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_bahasa->update($id, ['status' => 3]);
        redirect('bks_bahasa', 'refresh');
    }

    function detail_bks_bahasa($nim)
    {
        $data['bks_bahasa'] = $this->m_bks_bahasa->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_bahasa']) {
            $data['title'] = 'Detail Berkas' . $data['bks_bahasa']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_bahasa', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $this->form_validation->set_rules('periode', 'Periode', 'required');
            $this->form_validation->set_rules('tahun', 'tahun', 'required');
            $this->form_validation->set_rules('nama_bhs', 'Tahun Masuk', 'required');
            $this->form_validation->set_rules('skor', 'Tahun Keluar', 'required');
            $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
            $config['upload_path'] = './assets/berkas/bahasa/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['sk_bahasa'])) {
                $this->upload->do_upload('sk_bahasa');
                $data1 = $this->upload->data();
                $sk_bahasa = $data1['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $periode = $this->input->post('periode', TRUE);
                $tahun = $this->input->post('tahun', TRUE);
                $nama_bhs = $this->input->post('nama_bhs', TRUE);
                $skor = $this->input->post('skor', TRUE);
                $tanggal = $this->input->post('tanggal', TRUE);
                $data = [
                    'nim' => $nim,
                    'periode' => $periode,
                    'tahun' => $tahun,
                    'nama_bhs' => $nama_bhs,
                    'skor' => $skor,
                    'tanggal' => $tanggal,
                    'sk_bahasa' => $sk_bahasa,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();
                // var_dump($data);
                $insert = $this->db->insert('bks_bahasa', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_bahasa');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
