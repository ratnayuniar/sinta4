<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_jurusan');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['query'] = $this->m_jurusan->tampil_data();
		$data['title'] = 'SINTA PNM';
		$data['user'] = $this->db->get_where('user', ['email' =>
		$this->session->userdata('email')])->row_array();

		$this->load->view('templates/header', $data);
		$this->load->view('templates/sidebar', $data);
		$this->load->view('master_data/jurusan', $data);
		$this->load->view('templates/footer', $data);
	}

	public function add()
	{
		$id_jurusan = $this->input->post('id_jurusan');
		$this->m_jurusan->tambah_data();

		// if (empty($id_jurusan)) $this->m_jurusan->tambah_data();
		// else $this->m_jurusan->ubah_data($id_jurusan);
	}

	public function delete()
	{
		$id_jurusan = $this->input->post('id_jurusan2');
		$this->m_jurusan->hapus_data($id_jurusan);
	}
}
