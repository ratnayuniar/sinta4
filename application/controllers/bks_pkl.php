<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bks_pkl extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_bks_pkl');
        $this->load->helper('url');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['query'] = $this->m_bks_pkl->tampil_data();

        // $data['bks_seminar_user'] = $this->m_bks_seminar->bks_seminar_user();

        $data['title'] = 'SINTA PNM';
        $data['data'] = $this->db->get('bks_pkl')->result();

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berkas/bks_pkl', $data);
        $this->load->view('templates/footer', $data);
    }

    function save_bks_belum($id)
    {
        $this->m_bks_seminar->update($id, ['status' => 1]);
        redirect('bks_seminar', 'refresh');
    }

    function save_bks_kurang($id)
    {
        $this->m_bks_seminar->update($id, ['status' => 2]);
        redirect('bks_seminar', 'refresh');
    }

    function save_bks_lengkap($id)
    {
        $this->m_bks_seminar->update($id, ['status' => 3]);
        redirect('bks_seminar', 'refresh');
    }

    function detail_bks_pkl($nim)
    {
        $data['bks_pkl'] = $this->m_bks_pkl->get_nim($nim);

        $data['user'] = $this->db->get_where('user', ['email' =>
        $this->session->userdata('email')])->row_array();

        if ($data['bks_pkl']) {
            $data['title'] = 'Detail Berkas' . $data['bks_pkl']->nim;
            $this->load->view('templates/header', $data);
            $this->load->view('admin/sidebar', $data);
            $this->load->view('berkas/detail_bks_pkl', $data);
            $this->load->view('templates/footer', $data);
        }
    }

    public function create()
    {
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('nim', 'NIM', 'required');
            $this->form_validation->set_rules('judul', 'Judul', 'required');
            $this->form_validation->set_rules('tempat', 'Tempat', 'required');
            $this->form_validation->set_rules('provinsi', 'Provinsi', 'required');
            $this->form_validation->set_rules('kota', 'Kota', 'required');
            $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
            $this->form_validation->set_rules('ringkasan', 'Ringkasan', 'required');
            $config['upload_path'] = './assets/berkas/magang/';
            $config['allowed_types'] = 'pdf';
            $config['max_size']  = 2048;
            $config['encrypt_name']  = TRUE;

            $this->load->library('upload', $config);

            if (!empty($_FILES['sk_pkl'])) {
                $this->upload->do_upload('sk_pkl');
                $data1 = $this->upload->data();
                $sk_pkl = $data1['file_name'];
            }

            if (!empty($_FILES['laporan'])) {
                $this->upload->do_upload('laporan');
                $data2 = $this->upload->data();
                $laporan = $data2['file_name'];
            }

            if ($this->form_validation->run()) {
                $nim = $this->input->post('nim', TRUE);
                $judul = $this->input->post('judul', TRUE);
                $tempat = $this->input->post('tempat', TRUE);
                $provinsi = $this->input->post('provinsi', TRUE);
                $kota = $this->input->post('kota', TRUE);
                $tanggal = $this->input->post('tanggal', TRUE);
                $ringkasan = $this->input->post('ringkasan', TRUE);
                $data = [
                    'nim' => $nim,
                    'judul' => $judul,
                    'tempat' => $tempat,
                    'provinsi' => $provinsi,
                    'kota' => $kota,
                    'sk_pkl' => $sk_pkl,
                    'laporan' => $laporan,
                    'ringkasan' => $ringkasan,
                    'status' => 0,
                    'id_user' => $this->session->userdata('id_user')
                ];
                // print_r($data);
                // exit();
                // var_dump($data);
                $insert = $this->db->insert('bks_pkl', $data);
                if ($insert) {
                    $this->session->set_flashdata('pesan', '<div class="alert alert-success">Data berhasil disimpan</div>');
                    redirect('bks_pkl');
                }
            } else {
                $this->index();
            }
        } else {
            $this->index();
        }
    }
}
